@extends('layouts/layouts')

@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="{{url('/awal')}}">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="{{url('/training')}}">Manajemen Data Training</a></li>
		  <li class="breadcrumb-item active"><i>Tambah Data Training</i></li>
		</ol>

		<div class="card mb-3">
			<div class="card-header">
				<b>Tambah Data Training</b>				
			</div>
			<div class="card-body">
				<br>
				<div class="col-xl-12">		
					<form action="{{ route('training.insert') }}" class="form-horizontal" method="POST">
					{{ csrf_field() }}
						<div class="row">
							<div class="col-sm-3">Umur</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
								<input name="umur" class="form-control">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-3">Sistol</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
								<input name="sistol" class="form-control">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-3">Diastol</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
								<input name="diastol" class="form-control">
							</div>
						</div>
						<br>
						<br>
						<p><b>Riwayat Penyakit Ibu</b></p>
						<div class="row">
							<div class="col-sm-3">Penyakit Menular</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
			        			<select class="form-control" name="penyakit_menular_seksual" require>
		                            <option value="0">Tidak</option>
		                            <option value="1">Iya</option>
		                        </select>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-3">Mata Minus > 5</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
			        			<select class="form-control" name="mata_minus_dibawah_5" require>
		                            <option value="0">Tidak</option>
		                            <option value="1">Iya</option>
		                        </select>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-3">Asma</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
			        			<select class="form-control" name="asma" require>
		                            <option value="0">Tidak mengidap asma</option>
		                            <option value="1">Ringan Berkala</option>
		                            <option value="2">Ringan Menetap</option>
		                            <option value="3">Sedang Menetap</option>
		                            <option value="4">Parah Menetap</option>
		                        </select>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-3">Jantung</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
			        			<select class="form-control" name="jantung" require>
		                            <option value="0">Tidak</option>
		                            <option value="1">Iya</option>
		                        </select>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-3">Hipertensi</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
			        			<select class="form-control" name="hipertensi" require>
		                            <option value="0">Tidak memiliki Hipertensi</option>
		                            <option value="1">Hipertensi Ringan</option>
		                            <option value="2">Hipertensi Sedang</option>
		                            <option value="3">Hipertensi Berat</option>
		                        </select>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-3">Diabetes</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
			        			<select class="form-control" name="diabetes" require>
		                            <option value="0">Tidak Diabetes</option>
		                            <option value="1">Diabetes Tipe 1</option>
		                            <option value="2">Diabetes Tipe 2</option>
		                        </select>
							</div>
						</div>
						<br>
						<br>
						<p><b>Fisiologis Ibu</b></p>
						<div class="row">
							<div class="col-sm-3">Pinggul Kecil</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
			        			<select class="form-control" name="faktor_fisiologis_ibu" require>
		                            <option value="0">Tidak</option>
		                            <option value="1">Iya</option>
		                        </select>
							</div>
						</div>
						<br>
						<br>
						<p><b>Riwayat Kehamilan Sebelumnya</b></p>
						<div class="row">
							<div class="col-sm-3">Sesar</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
			        			<select class="form-control" name="operasi_dibawah_5" require>
		                            <option value="0">Tidak</option>
		                            <option value="1">Iya</option>
		                        </select>
							</div>
						</div>
						<br>
						<br>
						<p><b>Riwayat Kehamilan Saat Ini</b></p>
						<div class="row">
							<div class="col-sm-3">Plasenta Previa</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
			        			<select class="form-control" name="plasenta_previa" require>
		                            <option value="0">Tidak</option>
		                            <option value="1">Iya</option>
		                        </select>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-3">Bayi Sungsang</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
			        			<select class="form-control" name="sungsang" require>
		                            <option value="0">Tidak</option>
		                            <option value="1">Iya</option>
		                        </select>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-3">Bayi Kembar</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
			        			<select class="form-control" name="kembar" require>
		                            <option value="0">Tidak</option>
		                            <option value="1">Iya</option>
		                        </select>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-3">Detak Jantung Bayi Lemah</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
			        			<select class="form-control" name="jantung_bayi_lemah" require>
		                            <option value="0">Tidak</option>
		                            <option value="1">Iya</option>
		                        </select>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-3">Fetal Distress</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
			        			<select class="form-control" name="fetal_distress" require>
		                            <option value="0">Tidak</option>
		                            <option value="1">Iya</option>
		                        </select>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-3">Giant Baby</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
			        			<select class="form-control" name="giant_baby" require>
		                            <option value="0">Tidak</option>
		                            <option value="1">Iya</option>
		                        </select>
							</div>
						</div>
						<br>
						<br>
						<p><b>Hasil Proses Persalinan</b></p>
						<div class="row">
							<div class="col-sm-3">Hasil</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-8">
			        			<select class="form-control" name="giant_baby" require>
		                            <option value="0">Normal</option>
		                            <option value="1">Sesar</option>
		                        </select>
							</div>
						</div>
						<br>
						<br>
						<div class="form-group">
							<p align="right">
			        			<button class="btn btn-primary btn-md" type="submit">Simpan</button>
			        			<a href="{{ route('redis.list') }}"><button class="btn btn-danger btn-md" type="button">Batal</button></a>
			    			</p>

			    			<div class="col-sm-12" align="right">
			    			</div>
						</div>
					</form>
				</div>	
			</div>
		</div>
	</div>
</div>
@endsection