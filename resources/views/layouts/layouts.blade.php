<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Skripsi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <!-- <link rel="stylesheet" type="text/css" media="screen" href="main.css" /> -->
    <!-- Bootstrap core CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('public/asset/vendor/bootstrap/css/bootstrap.min.css')}}" />
    <!-- Custom fonts for this template-->
    <link rel="stylesheet" type="text/css" href="{{url('public/asset/vendor/font-awesome/css/font-awesome.min.css')}}" />
    <!-- Page level plugin CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('public/asset/datatables/dataTables.bootstrap4.css')}}">
    <!-- Custom styles for this template-->
    <link rel="stylesheet" type="text/css" href="{{url('public/asset/css/sb-admin.css')}}">
    <script src="main.js"></script>

</head>
  <!-- Navigation-->
<body class="fixed-nav sticky-footer bg-black" id="page-top">
  <nav class="navbar navbar-expand-lg navbar-dark bg-info fixed-top" id="mainNav">
    <a class="navbar-brand" href="{{url('/awal')}}">Admin</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{ url('/awal') }}">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Beranda</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Management Content">
          <a class="nav-link" href="{{ route('content.list') }}">
            <i class="fa fa-fw fa-th-list"></i>
            <span class="nav-link-text">Manajemen Konten</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Management Content">
          <a class="nav-link" href="{{ route('redis.list') }}">
            <i class="fa fa-fw fa-th-list"></i>
            <span class="nav-link-text">Manajemen Rekam<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Medis</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Management User">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-th-list"></i>
            <span class="nav-link-text">Manajemen User</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti">
            <li>
              <a href="{{ url('/admin') }}">Admin</a>
            </li>
            <li>
              <a href="{{ url('/bidan') }}">Bidan</a>
            </li>
            <li>
              <a href="{{ url('/pasien') }}">Pasien</a>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Bantuan">
          <a class="nav-link" href="{{ url('/training') }}">
            <i class="fa fa-fw fa-th-list"></i>
            <span class="nav-link-text">Manajemen Data Training</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Bantuan">
          <a class="nav-link" href="{{ url('/help') }}">
            <i class="fa fa-fw fa-link"></i>
            <span class="nav-link-text">Bantuan</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i><font color="white">Keluar</font></a>
        </li>
      </ul>
    </div>
  </nav>

  <!--CONTENT-->
  @yield('content')

</body>
</html>

    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Apakah anda ingin keluar?</h5>
           
          </div>
          <div class="modal-body"><i><font size="2">Pilih "Keluar" jika anda ingin keluar.</font></i></div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
            <a class="btn btn-primary" href="{{ route('auth.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Keluar</a>
            <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="{{url('public/asset/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{url('public/asset/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{url('public/asset/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <!-- Page level plugin JavaScript-->
    <script src="{{url('public/asset/vendor/chart.js/Chart.min.js')}}"></script>
    <script src="{{url('public/asset/vendor/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{url('public/asset/vendor/datatables/dataTables.bootstrap4.js')}}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{url('public/asset/js/sb-admin.min.js')}}"></script>
    <!-- Custom scripts for this page-->
    <script src="{{url('public/asset/js/sb-admin-datatables.min.js')}}"></script>
    <script src="{{url('public/asset/js/sb-admin-charts.min.js')}}"></script>