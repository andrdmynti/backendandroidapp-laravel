@extends('layouts/layouts')

@section('content')
<script type="text/javascript">
  function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete ?");
      if(x)
        return true;
      else
        return false;
    }   
</script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{url('/awal')}}">Beranda</a>
        </li>
        <li class="breadcrumb-item active"><i>Manajemen Konten</i></li>
      </ol>
      @if(session('status'))
        <div class="alert alert-success">
            <strong>{{ session('status') }}</strong>
        </div>
      @endif
      <a class="nav-link" href="{{ route('content.add')}}"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Konten</button></a>
      <hr>
      <div class="card mb-3">
      	<div class="card-header">
            <b>Manajemen Konten</b>
      	</div>
        <div class="card-body">
        	<div class="table-responsive">
        		<table class="table table-striped" width="100%" cellspacing="0">
        	    	
        	    	<thead>
        	      		<tr>
        	        		<th><font size="2px">No</font></th>
        	        		<th><font size="2px">Judul</font></th>
        	        		<th colspan="2"><font size="2px">Berita</font></th>
        	        		<th><font size="2px">Gambar</font></th>
        	        		<th><font size="2px">Tanggal Publish</font></th>
        	        		<th colspan="3"><center><font size="2px">Aksi</font></center></th>
        	      	</tr>
        	    	</thead>
        	    	<tbody>
                <?php $no = 1; ?>  
                @foreach($content as $show)
          				<tr>
          					<td><font size="2px">{{ $no++ }}</font></td>
          					<td><font size="2px">{{ $show->title }}</font></td>
          					<td width="380" colspan="2"><font size="2px"><p align="justify">{{ $show->news }}</p></font></td>
          					<td><img src="{{ url( 'storage/app/images/'.$show->picture) }}" height="50px" width="70px"></td>
          					<td><font size="2px">{{ $show->created_at }}</font></td>
          					<td>
          						<center>
          							<a data-toggle="tooltip" data-placement="top" title="Lihat Data" href="{{ url('/content/show')}}/{{$show->id}}">
                          <i class="fa fa-eye"></i>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Edit" href="{{ url('/content/edit')}}/{{$show->id}}">
                          <i class="fa fa-edit"></i>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Hapus" href="{{ url('/content/delete')}}/{{$show->id}}" type="button" onclick="ConfirmDelete()">
                          <i class="fa fa-trash"></i>
                       </a>
          						</center>
          					</td>
          				</tr>
                 @endforeach     	
        	    	</tbody>
        	  	</table>
        	 </div>
        </div>
      </div>
    </div>
</div>
@endsection