@extends('layouts/layouts')

@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="{{url('/awal')}}">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="{{ url('/content') }}">Manajemen Konten</a></li>
		  <li class="breadcrumb-item active"><i>Detail Konten</i></li>
		</ol>
		<div class="card mb-3">
	      	<div class="card-header">
	      		<b>Detail Konten</b>
	      	</div>
	        <div class="card-body">
				<div class="col-sm-12">		
					<form class="form-horizontal">
						{{ csrf_field() }}
						<input type="hidden" name="id" value="{{ $content->id }}">
						<table class="table table-bordered">
							<tr>
								<td width="150px"><b><font size="2px">Judul</font></b></td>
								<td><font size="2px"><i>{{ $content->title }}</i></font></td>
							</tr>
							<tr>
								<td width="150px"><b><font size="2px">Berita</b></td>
								<td><p align="justify"><font size="2px"><i>{{ $content->news }}</i></font></p></td>
							</tr>
							<tr>
								<td width="150px"><b><font size="2px">Gambar</b></td>
								<td><img src="{{ url( 'storage/app/images/'.$content->picture) }}" width="200px" height="120px"></td>
							</tr>
							<tr>
								<td width="150px"><b><font size="2px">Tanggal Publish</b></td>
								<td><font size="2px"><i>{{ $content->created_at }}</i></font></td>
							</tr>
						<table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection