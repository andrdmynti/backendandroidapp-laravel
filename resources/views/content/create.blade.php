@extends('layouts/layouts')

@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="{{url('/awal')}}">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="{{url('/content')}}">Manajemen Konten</a></li>
		  <li class="breadcrumb-item active"><i>Tambah Konten</i></li>
		</ol>
		<div class="card mb-3">
      		<div class="card-header">
      			<b>Tambah Konten</b>
      		</div>
		    <div class="card-body">
				<br>
				<div class="col-sm-12">		
					<form action="{{ route('content.insert') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
						<div class="row">
						    <div class="col-sm-2">Judul</div>
						    <div class="col-sm-1">:</div>
						    <div class="col-sm-9"><input type="text" class="form-control" name="title" placeholder="Judul" required></div>
						  </div>
						  <br>
						  <div class="row">
						    <div class="col-sm-2">Berita</div>
						    <div class="col-sm-1">:</div>
						    <div class="col-sm-9"><textarea class="form-control" name="news" placeholder="News" required></textarea></div>
						  </div>
						  <br>
						  <div class="row">
						    <div class="col-sm-2">Gambar</div>
						    <div class="col-sm-1">:</div>
						    <div class="col-sm-9"><input class="form-control" type="file" name="picture" id="fileToUpload" required></div>
						  </div>
						<br>
						<div class="form-group">
			    			<p align="right">
			        			<button type="submit" class="btn btn-primary">Simpan</button>
			        			<a href="{{ route('content.list') }}"><button class="btn btn-danger" type="button">Batal</button></a>
			    			</p>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection