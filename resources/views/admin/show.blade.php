@extends('layouts/layouts')

@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="{{url('/awal')}}">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="{{ url('/admin') }}">Manajemen Admin</a></li>
		  <li class="breadcrumb-item active">Detail Admin</li>
		</ol>
		<div class="card mb-3">
			<div class="card-header">
				<b>Detail Admin</b>
			</div>
			<div class="card-body">
				<div class="col-sm-12">		
					<form class="form-horizontal">
						{{ csrf_field() }}
						<input type="hidden" name="id" value="{{ $admin->id }}">
						<table class="table table-bordered">
							<tr>
								<td width="200px">Nama</td>
								<td>{{ $admin->name }}</td>
							</tr>
							<tr>
								<td width="200px">Username</td>
								<td>{{ $admin->username }}</td>
							</tr>
							<tr>
								<td width="200px">Email</td>
								<td>{{ $admin->email }}</td>
							</tr>
							<tr>
								<td width="200px">Level</td>
								<td>
									@if($admin->level == 1)
	                            		Admin
	                          		@endif
								</td>
							</tr>
						<table>
					</form>
				</div>	
			</div>
		</div>
		<br>
	</div>
</div>
@endsection