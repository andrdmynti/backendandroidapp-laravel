@extends('layouts/layouts')

@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="{{url('/awal')}}">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="{{url('/admin')}}">Manajemen Admin</a></li>
		  <li class="breadcrumb-item active"><i>Edit Admin</i></li>
		</ol>
		<div class="card mb-3">
      		<div class="card-header">
      			<b>Edit Admin</b>
      		</div>
		    <div class="card-body">
		<br>
		<div class="col-sm-12">		
			<form action="{{url('/admin/update')}}/{{$admin->id}}" class="form-horizontal" method="POST">
			{{ csrf_field() }}

			<input type="hidden" name="id" value="{{ $admin->id }}">
				<div class="row">
					    <div class="col-sm-2">Nama</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="text" class="form-control" name="name" value="{{ $admin->name }}"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-2">Username</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input class="form-control" name="username" value="{{ $admin->username }}"></input></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-2">Email</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input class="form-control" name="email" value="{{ $admin->email }}"></input></div>
				</div>
				<br>
				<div class="form-group">
	    			<p align="right">
	        			<button class="btn btn-danger" type="submit">Edit</button>
	    			</p>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection