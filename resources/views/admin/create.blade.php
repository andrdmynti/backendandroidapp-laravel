@extends('layouts/layouts')

@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="{{url('/awal')}}">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="{{url('/admin')}}">Manajemen Admin</a></li>
		  <li class="breadcrumb-item active"><i>Tambah Admin</i></li>
		</ol>
		<div class="card mb-3">
      		<div class="card-header">
      			<b>Tambah Admin</b>
      		</div>
		    <div class="card-body">
		<br>
		<div class="col-sm-12">		
			<form action="{{ route('admin.insert') }}" class="form-horizontal" method="POST">
			{{ csrf_field() }}
				<div class="row">
						    <div class="col-sm-2">Nama</div>
						    <div class="col-sm-1">:</div>
						    <div class="col-sm-9"><input type="text" class="form-control" name="name" placeholder="Nama Lengkap" require></div>
				</div>
				<br>
				<div class="row">
						    <div class="col-sm-2">Username</div>
						    <div class="col-sm-1">:</div>
						    <div class="col-sm-9"><input class="form-control" name="username" placeholder="Username" require></div>
				</div>
				<br>
				<div class="row">
						    <div class="col-sm-2">Email</div>
						    <div class="col-sm-1">:</div>
						    <div class="col-sm-9"><input class="form-control" name="email" placeholder="Email" require></div>
				</div>
				<br>
				<div class="row">
						    <div class="col-sm-2">Password</div>
						    <div class="col-sm-1">:</div>
						    <div class="col-sm-9"><input class="form-control" name="password" placeholder="Password" require></div>
				</div>
				<br>
				<div class="form-group">
	    			<div>
	        			<input type="hidden" name="level" value="1">
	    			</div>
				</div>
				<div class="form-group">
	    			<p align="right">
	        			<button class="btn btn-primary" type="submit">Simpan</button>
	    			</p>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection