@extends('layouts/layouts')

@section('content')
<script type="text/javascript">
  function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete ?");
      if(x)
        return true;
      else
        return false;
    }   
</script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{url('/awal')}}">Beranda</a>
        </li>
        <li class="breadcrumb-item active"><i>Manajemen Admin</i></li>
      </ol>
			@if(session('status'))
				<div class="alert alert-success">
					<strong>{{ session('status') }}</strong>
				</div>
			@endif
      <a class="nav-link" href="{{ route ('admin.add') }}"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Admin</button></a>
      <hr>
      <div class="card mb-3">
      	<div class="card-header">
          <b>Manajemen Admin</b>
      	</div>
        <div class="card-body">
        	<div class="table-responsive">
        		<table class="table table-striped" width="100%" cellspacing="0">
        	    	<thead>
        	      		<tr>
        	        		<th><font size="3px">No</font></th>
        	        		<th><font size="3px">Nama</font></th>
        	        		<th><font size="3px">Username</font></th>
        	        		<th><font size="3px">Email</font></th>
        	        		<th><font size="3px"><center>Aksi</center></font></th>
        	      		</tr>
        	    	</thead>
        	    	<tbody>
								<?php $no = 1; ?>
								@foreach($admin as $show)
        				<tr>
        					<td><font size="3px">{{ $no++ }}</font></td>
        					<td><font size="3px">{{ $show->name }}</font></td>
        					<td><font size="3px">{{ $show->username }}</font></td>
        					<td><font size="3px">{{ $show->email }}</font></td>
        					<td>        						
										<center>
											<a data-toggle="tooltip" data-placement="top" title="LIhat Data" href="{{ url('/admin/show')}}/{{$show->id}}">
                        <i class="fa fa-eye"></i>
        							</a>
											<a data-toggle="tooltip" data-placement="top" title="Edit" href="{{ url('/admin/edit')}}/{{$show->id}}">
                        <i class="fa fa-edit"></i>
        							</a>
											<a type="button" onclick="ConfirmDelete()" data-toggle="tooltip" data-placement="top" title="Hapus" href="{{ url('/admin/delete') }}/{{ $show->id }}">
                        <i class="fa fa-trash"></i>
											</a>
										</center>
        					</td>
        				</tr>
								@endforeach    	
        	    	</tbody>
        	  	</table>
        	 </div>
        </div>
      </div>
    </div>
</div>


<script type="text/javascript">

</script>
@endsection