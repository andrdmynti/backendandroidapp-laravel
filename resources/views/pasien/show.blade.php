@extends('layouts/layouts')

@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="{{url('/awal')}}">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="{{ url('/pasien') }}">Manajemen Pasien</a></li>
		  <li class="breadcrumb-item active"><i>Detail Data Pasien</i></li>
		</ol>
		<div class="card mb-3">
			<div class="card-header">
				<b>Detail Data Pasien</b>
			</div>
			<div class="card body">
				<div class="col-sm-12">		
					<form class="form-horizontal">
						{{ csrf_field() }}
						<input type="hidden" name="id" value="{{ $pasien->id }}">
						<br>
						<table class="table table-bordered">
							<tr>
								<td width="250px">Nama Pasien</td>
								<td>{{ $pasien->nama_pasien }}</td>
							</tr>
							<tr>
								<td width="250px">Tempat Lahir Pasien</td>
								<td>{{ $pasien->tmpt_lahir }}</td>
							</tr>
							<tr>
								<td width="250px">Tanggal Lahir Pasien</td>
								<td>{{ $pasien->tgl_lahir }}</td>
							</tr>
							<tr>
								<td width="250px">Umur Pasien</td>
								<td>{{ $pasien->umur_pasien }}</td>
							</tr>
							<tr>
								<td width="250px">Golongan Darah Pasien</td>
								<td>{{ $pasien->gol_darah }}</td>
							</tr>
							<tr>
								<td width="250px">Alamat Pasien</td>
								<td>{{ $pasien->alamat }}</td>
							</tr>
							<tr>
								<td width="250px">No. Identitas Pasien</td>
								<td>{{ $pasien->no_identitas }}</td>
							</tr>
							<tr>
								<td width="250px">Nama Wali</td>
								<td>{{ $pasien->nama_wali }}</td>
							</tr>
							<tr>
								<td width="250px">No. Telepon Wali</td>
								<td>{{ $pasien->nohp_wali }}</td>
							</tr>
						<table>
					</form>
				</div>	
			</div>
		</div>
		<br>
	</div>
</div>
@endsection