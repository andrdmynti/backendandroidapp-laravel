@extends('index')

@section('content')
<script type="text/javascript">
  function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete ?");
      if(x)
        return true;
      else
        return false;
    }   
</script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{url('/awal')}}">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Manajemen User Pasien</li>
      </ol>
			@if(session('status'))
				<div class="alert alert-success">
					<strong>{{ session('status') }}</strong>
				</div>
			@endif
      <a class="nav-link"  href="{{ route ('pasien.add') }}"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data Pasien Baru</button></a>
      <hr>
      <div class="card mb-3">
      	<div class="card-header"><b>Manajemen User Pasien</b>
      	</div>
        <div class="card-body">
        	<div class="table-responsive">
        		<table class="table table-striped" width="100%" cellspacing="0">
        	    	<thead>
        	      		<tr>
											<th><font size="2px">No</font></th>
        	        		<th><font size="2px">Nama Pasien</font></th>
        	        		<th><font size="2px">Tempat Lahir</font></th>
        	        		<th><font size="2px">Tanggal Lahir</font></th>
        	        		<th><font size="2px">Umur</font></th>
        	        		<th><font size="2px">Gol. Darah</font></th>
        	        		<th><font size="2px">Alamat</font></th>
                      <th><font size="2px">No. Identitas</font></th>
                      <th><font size="2px">Nama Wali</font></th>
        	        		<th><font size="2px">No. Hp Wali</font></th>
        	        		<th><font size="2px"><center>Aksi</center></font></th>
        	      		</tr>
        	    	</thead>
        	    	<tbody>
								<?php $no = 1; ?>
								@foreach($pasien as $show)
        				<tr>
        					<td><font size="2px">{{ $no++ }}</font></td>
        					<td><font size="2px">{{ $show->nama_pasien }}</font></td>
        					<td><font size="2px">{{ $show->tmpt_lahir }}</font></td>
        					<td><font size="2px">{{ $show->tgl_lahir }}</font></td>
        					<td><font size="2px">{{ $show->umur_pasien }}</font></td>
        					<td><font size="2px">{{ $show->gol_darah }}</font></td>
        					<td><font size="2px">{{ $show->alamat }}</font></td>
                  <td><font size="2px">{{ $show->no_identitas }}</font></td>
                  <td><font size="2px">{{ $show->nama_wali }}</font></td>
                  <td><font size="2px">{{ $show->nohp_wali }}</font></td>
        					<td>        						
										<center>
											<a data-toggle="tooltip" data-placement="top" title="LIhat Data" href="{{ url('/pasien/show')}}/{{$show->id}}">
                        <i class="fa fa-eye"></i>
        							</a>
											<a data-toggle="tooltip" data-placement="top" title="Edit" href="{{ url('/pasien/edit')}}/{{$show->id}}">
												<i class="fa fa-edit"></i>
        							</a>
											<a type="button" onclick="ConfirmDelete()" data-toggle="tooltip" data-placement="top" title="Hapus" href="{{ url('/pasien/delete') }}/{{ $show->id }}">
                        <i class="fa fa-trash"></i>
											</a>
										</center>
        					</td>
        				</tr>
								@endforeach
								</tbody>
        	  	</table>
        	 </div>
        </div>
      </div>
    </div>
</div>
@endsection