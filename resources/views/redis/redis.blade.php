@extends('index')

@section('content')
<script type="text/javascript">
  function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete data Rekam Medis ?");
      if(x)
        return true;
      else
        return false;
    }   
</script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{url('/awal')}}">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Manajemen Rekam Medis</li>
      </ol>
			@if(session('status'))
				<div class="alert alert-success">
					<strong>{{ session('status') }}</strong>
				</div>
			@endif
      <a class="nav-link"  href="{{ route ('redis.add') }}"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data Rekam Medis</button></a>
      <hr>
      <div class="card mb-3">
      	<div class="card-header">
          	<b>Manajemen Rekam Medis</b>
      	</div>
        <div class="card-body">
        	<div class="table-responsive">
        		<table class="table table-striped" width="100%" cellspacing="0">
        	    	<thead>
        	      		<tr>
                      <th><font size="2px">No</font></th>
                      <th><font size="2px">Nama Pasien</font></th>
                      <th><font size="2px">Nama Bidan</font></th>
                      <th><font size="2px">Tanggal Checkup</font></th>
                      <th><font size="2px">Minggu Ke-</font></th>
                      <th><font size="2px">Sistol</font></th>
        	        		<th><font size="2px">Diastol</font></th>
        	        		<th><font size="2px">Penyakit Menular</font></th>
        	        		<th><font size="2px">Mata Minus</font></th>
        	        		<th><font size="2px">Asma</font></th>
        	        		<th><font size="2px">Jantung</font></th>
        	        		<th><font size="2px">Hipertensi</font></th>
        	        		<th><font size="2px">Diabetes</font></th>
                      <th><font size="2px">Sesar</font></th>
        	        		<th><font size="2px">Pinggul Kecil</font></th>
        	        		<th><font size="2px">Plasenta Previa</font></th>
        	        		<th><font size="2px">Bayi Sungsang</font></th>
        	        		<th><font size="2px">Bayi Kembar</font></th>
        	        		<th><font size="2px">Detak Jantung Bayi Lemah</font></th>
        	        		<th><font size="2px">Fetal Distress</font></th>
        	        		<th><font size="2px">Baby Giant</font></th>
        	        		<th><center><font size="2px">Action</font></center></th>
        	      		</tr>
        	    	</thead>
        	    	<tbody>
                  <?php $no = 1; ?>
                  @foreach($redis as $show)
                    <tr>
                      <td><font size="2px">{{ $no++ }}</font></td>
                      <td><font size="2px">{{ $show->pasien->nama_pasien }}</font></td>
                      <td><font size="2px">{{ $show->bidan->nama_bidan }}</font></td>
                      <td><font size="2px">{{ $show->created_at }}</font></td>
                      <td><font size="2px">{{ $show->minggu_ke }}</font></td>
                      <td><font size="2px">{{ $show->sistol }}</font></td>
                      <td><font size="2px">{{ $show->diastol }}</font></td>
                      <td>
                        <font size="2px">
                          @if($show->penyakit_menular == 0)
                            Tidak
                          @else
                            Iya
                          @endif
                        </font>
                      </td>
                      <td>
                        <font size="2px">
                          @if($show->mata_minus == 0)
                            Tidak
                          @else
                            Iya
                          @endif
                        </font>
                      </td>
                      <td>
                        <font size="2px">
                          @if($show->asma == 1)
                            Ringan Berkala
                          @elseif($show->asma == 2 )
                            Ringan Menetap
                          @elseif($show->asma == 3 )
                            Sedang Menetap
                          @else
                            Sedang Menetap
                          @endif
                        </font>
                      </td>
                      <td>
                        <font size="2px">
                          @if($show->jantung == 0)
                            Tidak
                          @else
                            Iya
                          @endif
                        </font>
                      </td>
                      <td>
                        <font size="2px">
                          @if($show->hipertensi == 1)
                            Hipertensi Ringan
                          @elseif($show->hipertensi == 2 )
                            Hipertensi Sedang
                          @else
                            Hipertensi Berat
                          @endif
                        </font>
                      </td>
                      <td>
                        <font size="2px">
                          @if($show->diabetes == 1)
                            Diabetes Tipe 1
                          @else
                            Diabetes Tipe 2
                          @endif
                        </font>
                      </td>
                      <td>
                        <font size="2px">
                          @if($show->sesar == 0)
                            Tidak
                          @else
                            Iya
                          @endif
                        </font>
                      </td>
                      <td>
                        <font size="2px">
                          @if($show->pinggul == 0)
                            Tidak
                          @else
                            Iya
                          @endif
                        </font>
                      </td>
                      <td>
                        <font size="2px">
                          @if($show->p_previa == 0)
                            Tidak
                          @else
                            Iya
                          @endif
                        </font>
                      </td>
                      <td>
                        <font size="2px">
                          @if($show->b_sungsang == 0)
                            Tidak
                          @else
                            Iya
                          @endif
                        </font>
                      </td>
                      <td>
                        <font size="2px">
                          @if($show->b_kembar == 0)
                            Tidak
                          @else
                            Iya
                          @endif
                        </font>
                      </td>
                      <td>
                        <font size="2px">
                          @if($show->b_jantung_lemah == 0)
                            Tidak
                          @else
                            Iya
                          @endif
                        </font>
                      </td>
                      <td>
                        <font size="2px">
                          @if($show->fetal_distress == 0)
                            Tidak
                          @else
                            Iya
                          @endif
                        </font>
                      </td>
                      <td>
                        <font size="2px">
                          @if($show->b_giant == 0)
                            Tidak
                          @else
                            Iya
                          @endif
                        </font>
                      </td>
                      <td>        						
                        <center>
                            <a data-toggle="tooltip" data-placement="top" title="LIhat Data" href="{{ url('/redis/show')}}/{{$show->id}}">
                              <i class="fa fa-eye"></i>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Edit" href="{{ url('/redis/edit')}}/{{$show->id}}">
                              <i class="fa fa-edit"></i>
                            </a>
                            <a type="button" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="ConfirmDelete()" href="{{ url('/redis/delete') }}/{{ $show->id }}">
                              <i class="fa fa-trash"></i>
                            </a>
                        </center>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
        	  	</table>
        	 </div>
        </div>
      </div>
    </div>
</div>
@endsection