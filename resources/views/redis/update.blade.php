@extends('layouts/layouts')

@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="{{url('/awal')}}">Dashboard</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="{{url('/redis')}}">Manajemen Rekam Medis Pasien</a></li>
		  <li class="breadcrumb-item active"><i>Update Data Pasien</i></li>
		</ol><div class="card mb-3">
      		<div class="card-header">
      			<b>Update Data Pasien</b>
      		</div>
		    <div class="card-body">
			<br>
		<div class="col-sm-12">		
			<form action="{{url('/redis/update')}}/{{$redis->id}}" class="form-horizontal" method="POST">
			{{ csrf_field() }}

			<input type="hidden" name="id" value="{{ $redis->id }}">
				<div class="row">
				    <div class="col-sm-3">Nama Pasien</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8"><input type="text" class="form-control" name="nama_pasien" value="{{ $redis->pasien->nama_pasien }}" disabled></div>
				</div>
				<br>
				<div class="row">
				    <div class="col-sm-3">Nama Bidan</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8"><input type="text" class="form-control" name="nama_bidan" value="{{ $redis->bidan->nama_bidan }}" disabled></div>
				</div>
				<br>
				<br>
				<p><b>Tekanan Darah</b></p>
				<div class="row">
				    <div class="col-sm-3">Sistol</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8"><input type="text" class="form-control" name="sistol" value="	{{ $redis->sistol }}"></div>
				</div>
				<br>
				<div class="row">
				    <div class="col-sm-3">Diastol</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8"><input type="text" class="form-control" name="diastol" value="{{ $redis->diastol }}"></div>
				</div>
				<br>
				<br>
				<div class="row">
				    <div class="col-sm-3">Minggu Ke-</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8"><input type="text" class="form-control" name="minggu_ke" value="{{ $redis->minggu_ke }}"></div>
				</div>
				<br>
				<br>
				<p><b>Riwayat Penyakit</b></p>
				<div class="row">
				    <div class="col-sm-3">Penyakit Menular Seksual</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8">
				    	<select class="form-control" name="penyakit_menular">
				    		<option value="{{ $redis->penyakit_menular }}">@if ($redis->penyakit_menular==0) Tidak @else Iya @endif</option>
				    	</select>
				    </div>
				</div>
				<br>	
				<div class="row">
				    <div class="col-sm-3">Mata Minus</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8">
				    	<select class="form-control" name="mata_minus">
				    		<option value="{{ $redis->mata_minus }}">
				    			@if ($redis->mata_minus==0)
				    				Tidak 
				    			@else 
				    				Iya 
				    			@endif
				    		</option>
				    	</select>
				    </div>
				</div>
				<br>	
				<div class="row">
				    <div class="col-sm-3">Asma</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8">
				    	<select class="form-control" name="asma" value="@if($redis->asma == 0) Tidak @else Iya @endif">
				    		<option value="{{ $redis->asma }}">
				    			@if ($redis->asma==0) 
				    				Tidak mengidap Asma
				    			@elseif ($redis->asma==1) 
				    				Ringan Berkala
				    			@elseif ($redis->asma==2) 
				    				Ringan Menetap
				    			@elseif ($redis->asma==3) 
				    				Sedang Menetap 
				    			@else
				    				Parah Menetap
				    			@endif
				    		</option>
				    	</select> 
				    </div>
				</div>
				<br>
				<div class="row">
				    <div class="col-sm-3">Jantung</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8">
				    	<select class="form-control" name="jantung">
				    		<option value="{{ $redis->jantung }}">@if ($redis->jantung==0) Tidak @else Iya @endif</option>
				    	</select>
				    </div>
				</div>
				<br>
				<div class="row">
				    <div class="col-sm-3">Hipertensi</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8">
				    	<select class="form-control" name="hipertensi">
				    		<option value="{{ $redis->hipertensi }}">
				    			@if ($redis->hipertensi==0)
				    				Tidak memiliki Hipertensi
				    			@elseif ($redis->hipertensi==1)
				    				Hipertensi Ringan
				    			@elseif ($redis->hipertensi==2)
				    				Hipertensi Sedang
				    			@else
				    				Hipertensi Berat
				    			@endif
				    		</option>
				    	</select>
				    </div>
				</div>
				<br>
				<div class="row">
				    <div class="col-sm-3">Diabetes</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8">
				    	<select class="form-control" name="diabetes">
				    		<option value="{{ $redis->diabetes }}">
				    			@if ($redis->diabetes==0)
				    				Tidak Diabetes
				    			@elseif ($redis->diabetes==1)
				    				Diabetes Tipe 1
				    			@else
				    				Diabetes Tipe
				    			@endif
							</option>
				    	</select>
				    </div>
				</div>
				<br>
				<br>
				<p><b>Riwayat Kehamilan Sebelumnya</b></p>
				<div class="row">
				    <div class="col-sm-3">Sesar</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8">
				    	<select class="form-control" name="sesar">
				    		<option value="{{ $redis->sesar }}">
				    			@if ($redis->sesar==0)
				    				Tidak 
				    			@else 
				    				Iya 
				    			@endif
				    		</option>
				    	</select>
				    </div>
				</div>
				<br>
				<br>
				<p><b>Riwayat Kehamilan Saat Ini</b></p>
				<div class="row">
				    <div class="col-sm-3">Plasenta Previa</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8">
				    	<select class="form-control" name="p_previa">
				    		<option value="{{ $redis->p_previa }}">
				    			@if ($redis->p_previa==0)
				    				Tidak 
				    			@else 
				    				Iya 
				    			@endif
				    		</option>
				    	</select>
				    </div>
				</div>
				<br>
				<div class="row">
				    <div class="col-sm-3">Posisi Bayi Sungsang</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8">
				    	<select class="form-control" name="b_sungsang">
				    		<option value="{{ $redis->b_sungsang }}">
				    			@if ($redis->b_sungsang==0)
				    				Tidak 
				    			@else 
				    				Iya 
				    			@endif
				    		</option>
				    	</select>
				    </div>
				</div>
				<br>
				<div class="row">
				    <div class="col-sm-3">Bayi Kembar</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8">
				    	<select class="form-control" name="b_kembar">
				    		<option value="{{ $redis->b_kembar }}">
				    			@if ($redis->b_kembar==0)
				    				Tidak 
				    			@else 
				    				Iya 
				    			@endif
				    		</option>
				    	</select>
				    </div>
				</div>
				<br>
				<div class="row">
				    <div class="col-sm-3">Detak Jantung Bayi Lemah</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8">
				    	<select class="form-control" name="b_jantung_lemah">
				    		<option value="{{ $redis->b_jantung_lemah }}">
				    			@if ($redis->b_jantung_lemah==0)
				    				Tidak 
				    			@else 
				    				Iya 
				    			@endif
				    		</option>
				    	</select>
				    </div>
				</div>
				<br>
				<div class="row">
				    <div class="col-sm-3">Fetal Distress</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8">
				    	<select class="form-control" name="fetal_distress">
				    		<option value="{{ $redis->fetal_distress }}">
				    			@if ($redis->fetal_distress==0)
				    				Tidak 
				    			@else 
				    				Iya 
				    			@endif
				    		</option>
				    	</select>
				    </div>
				</div>
				<br>
				<div class="row">
				    <div class="col-sm-3">Giant Baby</div>
				    <div class="col-sm-1">:</div>
				    <div class="col-sm-8">
				    	<select class="form-control" name="b_giant">
				    		<option value="{{ $redis->b_giant }}">
				    			@if ($redis->b_giant==0)
				    				Tidak 
				    			@else 
				    				Iya 
				    			@endif
				    		</option>
				    	</select>
				  	</div>
				</div>
				<br>
				<div class="form-group">
	    			<p align="right">
	        			<button class="btn btn-primary" type="submit">Update</button>
	    			</p>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection