@extends('layouts/layouts')

@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="{{url('/awal')}}">Dashboard</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="{{ url('/redis') }}">Management Rekam Medis Pasien</a></li>
		  <li class="breadcrumb-item active"><i>Detail Rekam Medis Pasien</i></li>
		</ol>
		<div class="card mb-3">
			<div class="card-header">
				<b>Detail Rekam Medis Pasien</b>
			</div>
			<div class="card-body">
				<div class="col-sm-12">
					<form class="form-horizontal">
						{{ csrf_field() }}
						<input type="hidden" name="id" value="{{ $redis->id }}">
						<table class="table">
							<tr>
								<td class="col-sm-4">Nama Pasien</td>
								<td class="col-sm-6">{{ $redis->pasien->nama_pasien }}</td>
							</tr>
							<tr>
								<td class="col-sm-4">Nama Bidan</td>
								<td class="col-sm-6">{{ $redis->bidan->nama_bidan }}</td>
							</tr>
							<tr>
								<td class="col-sm-4">Tanggal Checkup</td>
								<td class="col-sm-6">{{ $redis->created_at }}</td>
							</tr>
							<tr>
								<td class="col-sm-4">Minggu Ke-</td>
								<td class="col-sm-6">{{ $redis->minggu_ke }}</td>
							</tr>
							<tr>
								<td class="col-sm-4">Sistol</td>
								<td class="col-sm-6">{{ $redis->sistol }}</td>
							</tr>
							<tr>
								<td class="col-sm-4">Diastol</td>
								<td class="col-sm-6">{{ $redis->diastol }}</td>
							</tr>
							<tr>
								<td class="col-sm-4">Penyakit Menular Seksual</td>
								<td class="col-sm-6">
									@if($redis->penyakit_menular == 0)
		                          		Tidak
		                        	@else
		                          		Iya
		                        	@endif
								</td>
							</tr>
							<tr>
								<td class="col-sm-4">Mata Minus > 5</td>
								<td class="col-sm-6">
									@if($redis->mata_minus == 0)
		                          		Tidak
		                        	@else
		                          		Iya
		                        	@endif
								</td>
							</tr>
							<tr>
								<td class="col-sm-4">Asma</td>
								<td class="col-sm-6">
									@if ($redis->asma==0) 
					    				Tidak mengidap Asma
					    			@elseif ($redis->asma==1) 
					    				Ringan Berkala
					    			@elseif ($redis->asma==2) 
					    				Ringan Menetap
					    			@elseif ($redis->asma==3) 
					    				Sedang Menetap 
					    			@else
					    				Parah Menetap
					    			@endif
								</td>
							</tr>
							<tr>
								<td class="col-sm-4">Jantung</td>
								<td class="col-sm-6">
									@if($redis->jantung == 0)
		                          		Tidak
		                        	@else
		                          		Iya
		                        	@endif
								</td>
							</tr>
							<tr>
								<td class="col-sm-4">Hipertensi</td>
								<td class="col-sm-6">
									@if ($redis->hipertensi==0)
										Tidak memiliki Hipertensi
									@elseif ($redis->hipertensi==1)
										Hipertensi Ringan
									@elseif ($redis->hipertensi==2)
										Hipertensi Sedang
									@else
										Hipertensi Berat
									@endif
								</td>
							</tr>
							<tr>
								<td class="col-sm-4">Diabetes</td>
								<td class="col-sm-6">
									@if ($redis->diabetes==0)
					    				Tidak Diabetes
									@elseif ($redis->diabetes==1)
					    				Diabetes Tipe 1
					    			@else
					    				Diabetes Tipe 2
					    			@endif
								</td>
							</tr>
							<tr>
								<td class="col-sm-4">Operasi Sesar < 5 tahun</td>
								<td class="col-sm-6">
									@if($redis->sesar == 0)
		                          		Tidak
		                        	@else
		                          		Iya
		                        	@endif
								</td>
							</tr>
							<tr>
								<td class="col-sm-4">Pinggul Kecil</td>
								<td class="col-sm-6">
									@if($redis->sesar == 0)
		                          		Tidak
		                        	@else
		                          		Iya
		                        	@endif
								</td>
							</tr>
							<tr>
								<td class="col-sm-4">Plasenta Previa</td>
								<td class="col-sm-6">
									@if($redis->p_previa == 0)
		                          		Tidak
		                        	@else
		                          		Iya
		                        	@endif
								</td>
							</tr>
							<tr>
								<td class="col-sm-4">Posisi Bayi Sungsang</td>
								<td class="col-sm-6">
									@if($redis->b_sungsang == 0)
		                          		Tidak
		                        	@else
		                          		Iya
		                        	@endif
								</td>
							</tr>
							<tr>
								<td class="col-sm-4">Bayi Kembar</td>
								<td class="col-sm-6">
									@if($redis->b_kembar == 0)
		                          		Tidak
		                        	@else
		                          		Iya
		                        	@endif
								</td>
							</tr>
							<tr>
								<td class="col-sm-4">Detak Jantung Bayi Lemah</td>
								<td class="col-sm-6">
									@if($redis->b_jantung_lemah == 0)
		                          		Tidak
		                        	@else
		                          		Iya
		                        	@endif
								</td>
							</tr>
							<tr>
								<td class="col-sm-4">Fetal Distress</td>
								<td class="col-sm-6">
									@if($redis->fetal_distress == 0)
		                          		Tidak
		                        	@else
		                          		Iya
		                        	@endif
								</td>
							</tr>
							<tr>
								<td class="col-sm-4">Giant Baby</td>
								<td class="col-sm-6">
									@if($redis->b_giant == 0)
		                          		Tidak
		                        	@else
		                          		Iya
		                        	@endif
								</td>
							</tr>
						<table>
					</form>	
				</div>
			</div>
		</div>
	</div>
</div>
@endsection