 @extends('layouts/layouts')

@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="{{url('/awal')}}">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="{{url('/bidan')}}">Manajemen Bidan</a></li>
		  <li class="breadcrumb-item active"><i>Edit Bidan</i></li>
		</ol>
		<div class="card mb-3">
      		<div class="card-header">
      			<b>Edit Bidan</b>
      		</div>
		    <div class="card-body">
			<br>
		<div class="col-sm-12">		
			<form action="{{url('/bidan/update')}}/{{$bidan->id}}" class="form-horizontal" method="POST">
			{{ csrf_field() }}

			<input type="hidden" name="id" value="{{ $bidan->id }}">
				<div class="row">
					    <div class="col-sm-2">NIB</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="text" class="form-control" name="nib" value="{{ $bidan->nib }}"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-2">Nama</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="text" class="form-control" name="nama_bidan" value="{{ $bidan->nama_bidan }}"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-2">Tempat Lahir</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="text" class="form-control" name="tempat" value="{{ $bidan->tempat_lahir }}"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-2">Tanggal Lahir</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="date" class="form-control" name="tgl_lahir" value="{{ $bidan->tgl_lahir }}"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-2">Alamat</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><textarea type="text" class="form-control" name="alamat">{{ $bidan->alamat }}</textarea></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-2">Tanggal Masuk</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="date" class="form-control" name="alamat"></div>
				</div>
				<br>
				<div class="form-group">
	    			<div class="col-sm-12" align="right">
	        			<button class="btn btn-danger" type="submit">Edit</button>
	    			</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection