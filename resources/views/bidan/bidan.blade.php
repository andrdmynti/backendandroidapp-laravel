@extends('layouts/layouts')

@section('content')
<script type="text/javascript">
  function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete ?");
      if(x)
        return true;
      else
        return false;
    }   
</script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{url('/awal')}}">Beranda</a>
        </li>
        <li class="breadcrumb-item active"><i>Manajemen User Bidan</i></li>
      </ol>
      @if(session('status'))
        <div class="alert alert-success">
            <strong>{{ session('status') }}</strong>
        </div>
      @endif
      <a class="nav-link" href="{{ route ('bidan.add') }}"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Bidan</button></a>
      <hr>
      <div class="card mb-3">
      	<div class="card-header">
          	<b>Manajemen User Bidan</b>
      	</div>
        <div class="card-body">
        	<div class="table-responsive">
        		<table class="table table-striped" width="100%" cellspacing="0">
        	    	<thead>
        	      		<tr>
                      <th><font size="2px">No</font></th>
                      <th><font size="2px">NIB</font></th>
                      <th><font size="2px">Nama Bidan</font></th>
                      <th><font size="2px">Tempat Lahir Bidan</font></th>
                      <th><font size="2px">Tanggal Lahir Bidan</font></th>
                      <th><font size="2px">Alamat</font></th>
        	        		<th><font size="2px">Status</font></th>
        	        		<th><font size="2px"><center>Aksi</center></font></th>
        	      		</tr>
        	    	</thead>
        	    	<tbody>
                    <?php $no = 1; ?>
                    @foreach($bidan as $show)
                    <tr>
                      <td><font size="2px">{{ $no++ }}</td>
                      <td><font size="2px">{{ $show->nib }}</td>
                      <td><font size="2px">{{ $show->nama_bidan }}</font></td>
                      <td><font size="2px">{{ $show->tempat_lahir }}</font></td>
                      <td><font size="2px">{{ $show->tgl_lahir }}</font></td>
                      <td><font size="2px">{{ $show->alamat }}</font></td>
                      <td>
                        <font size="2px">
                          @if($show->status == 0)
                            Tidak Aktif
                          @else
                            Aktif
                          @endif  
                        </font>
                      </td>
                      <td>        						
                      <center>
          							<a  data-toggle="tooltip" data-placement="top" title="Lihat Data"href="{{ url('/bidan/show')}}/{{$show->id}}">
                          <i class="fa fa-eye"></i>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Edit" href="{{ url('/bidan/edit')}}/{{$show->id}}">
                          <i class="fa fa-edit"></i>
                        </a>
                        <a type="button" onclick="ConfirmDelete()" data-toggle="tooltip" data-placement="top" title="Hapus" href="{{ url('/bidan/delete')}}/{{$show->id}}">
                          <i class="fa fa-trash"></i>
                       </a>
          						</center>
                      </td>
                    </tr>
                    @endforeach
        	    	</tbody>
        	  	</table>
        	 </div>
        </div>
      </div>
    </div>
</div>
@endsection