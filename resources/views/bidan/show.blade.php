@extends('layouts/layouts')

@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="{{url('/awal')}}">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="{{ url('/bidan') }}">Manajemen Bidan</a></li>
		  <li class="breadcrumb-item active">Detail Bidan</li>
		</ol>
		<div class="card mb-3">
			<div class="card-header">
				<b>Detail Bidan</b>
			</div>
			<div class="card-body">
				<div class="col-sm-12">		
					<form class="form-horizontal">
						{{ csrf_field() }}
						<input type="hidden" name="id" value="{{ $bidan->bidan_id }}">
						<table class="table table-bordered">
							<tr>
								<td>NIB</td>
								<td>{{ $bidan->nib }}</td>
							</tr>
							<tr>
								<td>Nama</td>
								<td>{{ $bidan->nama_bidan }}</td>
							</tr>
							<tr>
								<td>Tempat Lahir</td>
								<td>{{ $bidan->tempat_lahir }}</td>
							</tr>
							<tr>
								<td>Tanggal Lahir</td>
								<td>{{ $bidan->tgl_lahir }}</td>
							</tr>
							<tr>
								<td>Alamat</td>
								<td>{{ $bidan->alamat }}</td>
							</tr>
							<tr>
								<td>Status</td>
								<td>
									@if($bidan->status == 0)
									  Tidak Aktif
									@else
									  Aktif
									@endif
								</td>
							</tr>
						<table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection