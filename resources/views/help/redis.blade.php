@extends('layouts/layouts')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{url('/awal')}}">Beranda</a>
        </li>
        <li class="breadcrumb-item active"><a href="{{url('/help')}}">Bantuan</a></li>
        <li class="breadcrumb-item active"><i>Cara mengubah pada konten manajemen rekam medis</i></li>
      </ol>

      		<div class="card mb-3">
      			<div class="card-header">
      				Cara menambah data rekam medis pada konten rekam medis				
      			</div>
      			<div class="card-body">
      				
      			</div>
      		</div>

      		<div class="card mb-3">
      			<div class="card-header" style="background-color: #5bb3c6;">
      				<font color="white">Cara melihat data rekam medis pada konten manajemen rekam medis</font>			
      			</div>
      			<div class="card-body">
      				<table class="table table-border">
      					<br>
      					<tr>
      						<td><i>- Buka halaman manajemen rekam medis.</i></td>
      					</tr>
      					<tr>
      						<td><i>- Pilih lihat data pada aksi sesuai data yang ingin dilihat.</i></td>
      					</tr>
      					<tr>
      						<td><i>- Lihat detail data</i></td>
      					</tr>
      					<tr>
      						<td><i>- Selesai</i></td>
      					</tr>
      				</table>
      			</div>
      		</div>

      		<div class="card mb-3">
      			<div class="card-header" style="background-color: #5bb3c6;">
      				Cara mengedit data rekam medis pada konten manajemen rekam medis				
      			</div>
      			<div class="card-body">
      				
      			</div>
      		</div>

      		<div class="card mb-3">
      			<div class="card-header" style="background-color: #5bb3c6;">
      				Cara mengapus data rekam medis pada konten manajemen rekam medis				
      			</div>
      			<div class="card-body">
      				
      			</div>
      		</div>
      	</div>
      </div>	
  </div>
</div>

@endsection