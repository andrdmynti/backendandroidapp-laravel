@extends('layouts/layouts')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="{{url('/awal')}}">Beranda</a>
        </li>
        <li class="breadcrumb-item active"><a href="{{url('/help')}}">Bantuan</a></li>
        <li class="breadcrumb-item active"><i>Cara mengubah pada konten manajemen user</i></li>
      </ol>

      		<div class="card mb-3">
      			<div class="card-header">
      				Cara menambah data bidan pada konten manajemen user				
      			</div>
      			<div class="card-body">
      				
      			</div>
      		</div>

      		<div class="card mb-3">
      			<div class="card-header" style="background-color: #5bb3c6;">
      				<font color="white">Cara melihat detail data bidan pada konten manajemen user</font>			
      			</div>
      			<div class="card-body">
      				<table class="table table-border">
      					<br>
      					<tr>
      						<td><i>- Klik menu manajemen user.</i></td>
      					</tr>
                <tr>
                  <td><i>- Pilih menu bidan.</i></td>
                </tr>
      					<tr>
      						<td><i>- Pilih lihat data pada aksi sesuai data yang ingin dilihat.</i></td>
      					</tr>
      					<tr>
      						<td><i>- Lihat detail data</i></td>
      					</tr>
      					<tr>
      						<td><i>- Selesai</i></td>
      					</tr>
      				</table>
      			</div>
      		</div>

      		<div class="card mb-3">
      			<div class="card-header" style="background-color: #5bb3c6;">
      				Cara mengedit data bidan pada konten manajemen user				
      			</div>
      			<div class="card-body">
      				
      			</div>
      		</div>

      		<div class="card mb-3">
      			<div class="card-header" style="background-color: #5bb3c6;">
      				Cara mengapus data bidan pada konten manajemen user			
      			</div>
      			<div class="card-body">
      				
      			</div>
      		</div>
      	</div>
      </div>	
  </div>
</div>

@endsection