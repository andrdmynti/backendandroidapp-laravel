<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="<?php echo e(url('/awal')); ?>">Dashboard</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="<?php echo e(url('/admin')); ?>">Manajemen Data Pasien</a></li>
		  <li class="breadcrumb-item active"><i>Update Data Pasien</i></li>
		</ol><div class="card mb-3">
      		<div class="card-header">
      			<b>Update Data Pasien</b>
      		</div>
		    <div class="card-body">
			<br>
		<div class="col-sm-12">		
			<form action="<?php echo e(url('/redis/update')); ?>/<?php echo e($redis->id); ?>" class="form-horizontal" method="POST">
			<?php echo e(csrf_field()); ?>


			<input type="hidden" name="id" value="<?php echo e($redis->id); ?>">
				<div class="row">
					    <div class="col-sm-3">Nama Pasien</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="nama_pasien" value="<?php echo e($redis->pasien->nama_pasien); ?>" disabled></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">Nama Bidan</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="nama_bidan" value="<?php echo e($redis->bidan->nama_bidan); ?>" disabled></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">Tekanan Darah</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="tekanan_darah" value="<?php echo e($redis->tekanan_darah); ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">Penyakit Menular Seksual</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="penyakit_menular" value="<?php if($redis->penyakit_menular==0): ?> Tidak <?php else: ?> Iya <?php endif; ?>"></div>
				</div>
				<br>	
				<div class="row">
					    <div class="col-sm-3">Mata Minus</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="mata_minus" value="<?php if($redis->mata_minus == 0): ?> Tidak <?php else: ?> Iya <?php endif; ?>"></div>
				</div>
				<br>	
				<div class="row">
					    <div class="col-sm-3">Asma</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="asma" value="<?php if($redis->asma == 0): ?> Tidak <?php else: ?> Iya <?php endif; ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">Jantung</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="jantung" value="<?php if($redis->jantung==0): ?> Tidak <?php else: ?> Iya <?php endif; ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">Hipertensi</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="hipertensi" value="<?php if($redis->hipertensi == 0): ?> Tidak <?php else: ?> Iya <?php endif; ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">Diabetes</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="diabetes" value="<?php if($redis->diabetes == 0): ?> Tidak <?php else: ?> Iya <?php endif; ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">Sesar</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="sesar" value="<?php if($redis->sesar==0): ?> Tidak <?php else: ?> Iya <?php endif; ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">Plasenta Previa</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="p_previa" value="<?php if($redis->p_previa == 0): ?> Tidak <?php else: ?> Iya <?php endif; ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">Posisi Bayi Sungsang</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="b_sungsang" value="<?php if($redis->b_sungsang == 0): ?> Tidak <?php else: ?> Iya <?php endif; ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">Bayi Kembar</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="b_kembar" value="<?php if($redis->b_kembar == 0): ?> Tidak <?php else: ?> Iya <?php endif; ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">Detak Jantung Bayi Lemah</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="b_jantung_lemah" value="<?php if($redis->b_jantung_lemah == 0): ?> Tidak <?php else: ?> Iya <?php endif; ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">Fetal Distress</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="fetal_distress" value="<?php if($redis->fetal_distress == 0): ?> Tidak <?php else: ?> Iya <?php endif; ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">Giant Baby</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="b_giant" value="<?php if($redis->b_giant == 0): ?> Tidak <?php else: ?> Iya <?php endif; ?>"></div>
				</div>
				<br>
				<div class="form-group">
	    			<p align="right">
	        			<button class="btn btn-primary" type="submit">Update</button>
	    			</p>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>