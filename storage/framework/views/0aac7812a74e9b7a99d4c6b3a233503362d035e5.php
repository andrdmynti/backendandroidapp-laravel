<?php $__env->startSection('content'); ?>



<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="<?php echo e(url('/awal')); ?>">Bantuan</a>
		  </li>
		</ol>

		<div class="card mb-3">
			<div class="card-header">
				<b>Bantuan</b>				
			</div>
			<div class="card-body">
				<table class="table table-bordered">
				    <tbody>
				      <tr>
				        <td><a href="<?php echo e(url('/konten')); ?>"><button type="button" class="btn btn-info btn-lg"><i><font size="3px">
                    Cara mengubah konten pada manajemen konten</font></i></button></a></td>
				      </tr>
				      <tr>
				        <td><a href="<?php echo e(url('/remedis')); ?>"><button type="button" class="btn btn-info btn-lg"><i><font size="3px">
                    Cara mengubah data rekam medis pada manajemen rekam medis</font></i></button></a></td>
				      </tr>
				     <tr>
				       <td><a href="<?php echo e(url('/adminhelp')); ?>"><button type="button" class="btn btn-info btn-lg"><i><font size="3px">
                    Cara mengubah admin pada manajemen user</font></i></button></a></td>
				     </tr>
				     <tr> 
				       <td><a href="<?php echo e(url('/bidanhelp')); ?>"><button type="button" class="btn btn-info btn-lg"><i><font size="3px">
                    Cara mengubah bidan pada manajemen user</font></i></button></a></td>
				     </tr>
				     <tr>
				       <td><a href="<?php echo e(url('/pasienhelp')); ?>"><button type="button" class="btn btn-info btn-lg"><i><font size="3px">
                    Cara mengubah pasien pada manajemen user</font></i></button></a></td>
				     </tr>
				    </tbody>
				  </table>
			</div>
		</div>
	</div>
</div>	

  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-info">
          <p class="modal-title"><i>Cara menambah konten pada manajemen konten</i></p>
        </div>
        <div class="modal-body">
          <p>1. Buka halaman manajemen konten.
          	<br>
          	<br>
          	2. Klik pada button <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Konten</button> untuk menambah konten.
          	<br>
          	<br>
          	3. Isi judul, berita dan gambar pada form yang tersedia. Setelah itu klik pada button <button type="button" class="btn btn-primary">Simpan</button>.
          	<br>
          	<br>
          	4. Selesai.
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

    <div class="modal fade" id="myModal2" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header bg-info">
            <p class="modal-title"><i>Cara menambah data rekam medis pada manajemen rekam medis</i></p>
          </div>
          <div class="modal-body">
            <p>1. Buka halaman manajemen rekam medis.
            	<br>
            	<br>
            	2. Klik pada button <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data Rekam Medis</button> untuk menambah data rekam medis.
            	<br>
            	<br>
            	3. Isi form yang tersedia. Setelah itu klik pada button <button type="button" class="btn btn-primary">Simpan</button>.
            	<br>
            	<br>
            	4. Selesai.
            </p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

      <div class="modal fade" id="myModal3" role="dialog">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header bg-info">
              <p class="modal-title"><i>Cara menambah admin pada manajemen user</i></p>
            </div>
            <div class="modal-body">
              <p>1. Buka halaman manajemen user. Pilih Admin.
              	<br>
              	<br>
              	2. Klik pada button <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Admin</button> untuk menambah admin.
              	<br>
              	<br>
              	3. Isi nama, username, email dan password pada form yang tersedia. Setelah itu klik pada button <br>&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary">Simpan</button>.
              	<br>
              	<br>
              	4. Selesai.
              </p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

        <div class="modal fade" id="myModal4" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header bg-info">
                <p class="modal-title"><i>Cara menambah bidan pada manajemen user</i></p>
              </div>
              <div class="modal-body">
                <p>1. Buka halaman manajemen user. Pilih Bidan.
                	<br>
                	<br>
                	2. Klik pada button <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Bidan</button> untuk menambah bidan.
                	<br>
                	<br>
                	3. Isi form yang tersedia. Setelah itu klik pada button <button type="button" class="btn btn-primary">Simpan</button>.
                	<br>
                	<br>
                	4. Selesai.
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

          <div class="modal fade" id="myModal5" role="dialog">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header bg-info">
                  <p class="modal-title"><i>Cara menambah pasien pada manajemen user</i></p>
                </div>
                <div class="modal-body">
                  <p>1. Buka halaman manajemen user. Pilih Pasien.
                  	<br>
                  	<br>
                  	2. Klik pada button <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Pasien</button> untuk menambah pasien.
                  	<br>
                  	<br>
                  	3. Isi form yang tersedia. Setelah itu klik pada button <button type="button" class="btn btn-primary">Simpan</button>.
                  	<br>
                  	<br>
                  	4. Selesai.
                  </p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>