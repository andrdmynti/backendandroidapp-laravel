<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="<?php echo e(url('/awal')); ?>">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="<?php echo e(url('/admin')); ?>">Manajemen Admin</a></li>
		  <li class="breadcrumb-item active"><i>Edit Admin</i></li>
		</ol>
		<div class="card mb-3">
      		<div class="card-header">
      			<b>Edit Admin</b>
      		</div>
		    <div class="card-body">
		<br>
		<div class="col-sm-12">		
			<form action="<?php echo e(url('/admin/update')); ?>/<?php echo e($admin->id); ?>" class="form-horizontal" method="POST">
			<?php echo e(csrf_field()); ?>


			<input type="hidden" name="id" value="<?php echo e($admin->id); ?>">
				<div class="row">
					    <div class="col-sm-2">Nama</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="text" class="form-control" name="name" value="<?php echo e($admin->name); ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-2">Username</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input class="form-control" name="username" value="<?php echo e($admin->username); ?>"></input></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-2">Email</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input class="form-control" name="email" value="<?php echo e($admin->email); ?>"></input></div>
				</div>
				<br>
				<div class="form-group">
	    			<p align="right">
	        			<button class="btn btn-danger" type="submit">Edit</button>
	    			</p>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>