<?php $__env->startSection('content'); ?>

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo e(url('/awal')); ?>">Beranda</a>
        </li>
        <li class="breadcrumb-item active"><a href="<?php echo e(url('/help')); ?>">Bantuan</a></li>
        <li class="breadcrumb-item active"><i>Cara mengubah pada konten manajemen konten</i></li>
      </ol>

      		<div class="card mb-3">
      			<div class="card-header">
      				Cara menambah konten pada konten manajemen konten				
      			</div>
      			<div class="card-body">
      				
      			</div>
      		</div>

      		<div class="card mb-3">
      			<div class="card-header" style="background-color: #5bb3c6;">
      				<font color="white">Cara melihat konten pada konten manajemen konten</font>			
      			</div>
      			<div class="card-body">
      				<table class="table table-border">
      					<br>
      					<tr>
      						<td><i>- Buka halaman manajemen konten.</i></td>
      					</tr>
      					<tr>
      						<td><i>- Pilih lihat data pada aksi sesuai data yang ingin dilihat.</i></td>
      					</tr>
      					<tr>
      						<td><i>- Lihat detail data</i></td>
      					</tr>
      					<tr>
      						<td><i>- Selesai</i></td>
      					</tr>
      				</table>
      			</div>
      		</div>

      		<div class="card mb-3">
      			<div class="card-header" style="background-color: #5bb3c6;">
      				Cara mengedit konten pada konten manajemen konten				
      			</div>
      			<div class="card-body">
      				
      			</div>
      		</div>

      		<div class="card mb-3">
      			<div class="card-header" style="background-color: #5bb3c6;">
      				Cara mengapus konten pada konten manajemen konten				
      			</div>
      			<div class="card-body">
      				
      			</div>
      		</div>
      	</div>
      </div>	
  </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>