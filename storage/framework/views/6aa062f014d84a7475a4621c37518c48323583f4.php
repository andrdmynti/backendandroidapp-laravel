<?php $__env->startSection('content'); ?>
<script type="text/javascript">
  function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete ?");
      if(x)
        return true;
      else
        return false;
    }   
</script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo e(url('/awal')); ?>">Beranda</a>
        </li>
        <li class="breadcrumb-item active"><i>Manajemen Admin</i></li>
      </ol>
			<?php if(session('status')): ?>
				<div class="alert alert-success">
					<strong><?php echo e(session('status')); ?></strong>
				</div>
			<?php endif; ?>
      <a class="nav-link" href="<?php echo e(route ('admin.add')); ?>"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Admin</button></a>
      <hr>
      <div class="card mb-3">
      	<div class="card-header">
          <b>Manajemen Admin</b>
      	</div>
        <div class="card-body">
        	<div class="table-responsive">
        		<table class="table table-striped" width="100%" cellspacing="0">
        	    	<thead>
        	      		<tr>
        	        		<th><font size="3px">No</font></th>
        	        		<th><font size="3px">Nama</font></th>
        	        		<th><font size="3px">Username</font></th>
        	        		<th><font size="3px">Email</font></th>
        	        		<th><font size="3px"><center>Aksi</center></font></th>
        	      		</tr>
        	    	</thead>
        	    	<tbody>
								<?php $no = 1; ?>
								<?php $__currentLoopData = $admin; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $show): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        				<tr>
        					<td><font size="3px"><?php echo e($no++); ?></font></td>
        					<td><font size="3px"><?php echo e($show->name); ?></font></td>
        					<td><font size="3px"><?php echo e($show->username); ?></font></td>
        					<td><font size="3px"><?php echo e($show->email); ?></font></td>
        					<td>        						
										<center>
											<a data-toggle="tooltip" data-placement="top" title="LIhat Data" href="<?php echo e(url('/admin/show')); ?>/<?php echo e($show->id); ?>">
                        <i class="fa fa-eye"></i>
        							</a>
											<a data-toggle="tooltip" data-placement="top" title="Edit" href="<?php echo e(url('/admin/edit')); ?>/<?php echo e($show->id); ?>">
                        <i class="fa fa-edit"></i>
        							</a>
											<a type="button" onclick="ConfirmDelete()" data-toggle="tooltip" data-placement="top" title="Hapus" href="<?php echo e(url('/admin/delete')); ?>/<?php echo e($show->id); ?>">
                        <i class="fa fa-trash"></i>
											</a>
										</center>
        					</td>
        				</tr>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    	
        	    	</tbody>
        	  	</table>
        	 </div>
        </div>
      </div>
    </div>
</div>


<script type="text/javascript">

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>