<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="<?php echo e(url('/awal')); ?>">Dashboard</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="<?php echo e(url('/redis')); ?>">Management Rekam Medis Pasien</a></li>
		  <li class="breadcrumb-item active"><i>Detail Rekam Medis Pasien</i></li>
		</ol>
		<div class="card mb-3">
			<div class="card-header">
				<b>Detail Rekam Medis Pasien</b>
			</div>
			<div class="card-body">
				<div class="col-sm-12">
					<form class="form-horizontal">
					<?php echo e(csrf_field()); ?>

					<input type="hidden" name="id" value="<?php echo e($redis->id); ?>">
					<table class="table table-bordered">
						<tr>
							<td class="col-sm-4">Nama Pasien</td>
							<td class="col-sm-6"><?php echo e($redis->pasien->nama_pasien); ?></td>
						</tr>
						<tr>
							<td class="col-sm-4">Nama Bidan</td>
							<td class="col-sm-6"><?php echo e($redis->bidan->nama_bidan); ?></td>
						</tr>
						<tr>
							<td class="col-sm-4">Tanggal Checkup</td>
							<td class="col-sm-6"><?php echo e($redis->created_at); ?></td>
						</tr>
						<tr>
							<td class="col-sm-4">Tekanan Darah</td>
							<td class="col-sm-6"><?php echo e($redis->tekanan_darah); ?></td>
						</tr>
						<tr>
							<td class="col-sm-4">Penyakit Menular Seksual</td>
							<td class="col-sm-6">
								<?php if($redis->penyakit_menular == 0): ?>
	                          		Tidak
	                        	<?php else: ?>
	                          		Iya
	                        	<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td class="col-sm-4">Mata Minus > 5</td>
							<td class="col-sm-6">
								<?php if($redis->mata_minus == 0): ?>
	                          		Tidak
	                        	<?php else: ?>
	                          		Iya
	                        	<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td class="col-sm-4">Asma</td>
							<td class="col-sm-6">
								<?php if($redis->asma == 0): ?>
	                          		Tidak
	                        	<?php else: ?>
	                          		Iya
	                        	<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td class="col-sm-4">Jantung</td>
							<td class="col-sm-6">
								<?php if($redis->jantung == 0): ?>
	                          		Tidak
	                        	<?php else: ?>
	                          		Iya
	                        	<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td class="col-sm-4">Hipertensi</td>
							<td class="col-sm-6">
								<?php if($redis->hipertensi == 0): ?>
	                          		Tidak
	                        	<?php else: ?>
	                          		Iya
	                        	<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td class="col-sm-4">Diabetes</td>
							<td class="col-sm-6">
								<?php if($redis->diabetes == 0): ?>
	                          		Tidak
	                        	<?php else: ?>
	                          		Iya
	                        	<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td class="col-sm-4">Operasi Sesar < 5 tahun</td>
							<td class="col-sm-6">
								<?php if($redis->sesar == 0): ?>
	                          		Tidak
	                        	<?php else: ?>
	                          		Iya
	                        	<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td class="col-sm-4">Pinggul Kecil</td>
							<td class="col-sm-6">
								<?php if($redis->sesar == 0): ?>
	                          		Tidak
	                        	<?php else: ?>
	                          		Iya
	                        	<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td class="col-sm-4">Plasenta Previa</td>
							<td class="col-sm-6">
								<?php if($redis->p_previa == 0): ?>
	                          		Tidak
	                        	<?php else: ?>
	                          		Iya
	                        	<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td class="col-sm-4">Posisi Bayi Sungsang</td>
							<td class="col-sm-6">
								<?php if($redis->b_sungsang == 0): ?>
	                          		Tidak
	                        	<?php else: ?>
	                          		Iya
	                        	<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td class="col-sm-4">Bayi Kembar</td>
							<td class="col-sm-6">
								<?php if($redis->b_kembar == 0): ?>
	                          		Tidak
	                        	<?php else: ?>
	                          		Iya
	                        	<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td class="col-sm-4">Detak Jantung Bayi Lemah</td>
							<td class="col-sm-6">
								<?php if($redis->b_jantung_lemah == 0): ?>
	                          		Tidak
	                        	<?php else: ?>
	                          		Iya
	                        	<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td class="col-sm-4">Fetal Distress</td>
							<td class="col-sm-6">
								<?php if($redis->fetal_distress == 0): ?>
	                          		Tidak
	                        	<?php else: ?>
	                          		Iya
	                        	<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td class="col-sm-4">Giant Baby</td>
							<td class="col-sm-6">
								<?php if($redis->b_giant == 0): ?>
	                          		Tidak
	                        	<?php else: ?>
	                          		Iya
	                        	<?php endif; ?>
							</td>
						</tr>
					<table>
				</form>	
				</div>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>