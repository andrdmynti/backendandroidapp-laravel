<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="<?php echo e(url('/awal')); ?>">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="<?php echo e(url('/content')); ?>">Manajemen Konten</a></li>
		  <li class="breadcrumb-item active"><i>Detail Konten</i></li>
		</ol>
		<div class="card mb-3">
	      	<div class="card-header">
	      		<b>Detail Konten</b>
	      	</div>
	        <div class="card-body">
				<div class="col-sm-12">		
					<form class="form-horizontal">
						<?php echo e(csrf_field()); ?>

						<input type="hidden" name="id" value="<?php echo e($content->id); ?>">
						<table class="table table-bordered">
							<tr>
								<td class="col-sm-1"><b><font size="2px">Judul</font></b></td>
								<td class="col-sm-11"><font size="2px"><i><?php echo e($content->title); ?></i></font></td>
							</tr>
							<tr>
								<td class="col-sm-1"><b><font size="2px">Berita</b></td>
								<td class="col-sm-11"><p align="justify"><font size="2px"><i><?php echo e($content->news); ?></i></font></p></td>
							</tr>
							<tr>
								<td class="col-sm-1"><b><font size="2px">Gambar</b></td>
								<td class="col-sm-11"><img src="<?php echo e(url( 'storage/app/images/'.$content->picture)); ?>" width="200px" height="120px"></td>
							</tr>
							<tr>
								<td class="col-sm-4"><b><font size="2px">Tanggal Publish</b></td>
								<td class="col-sm-6"><font size="2px"><i><?php echo e($content->created_at); ?></i></font></td>
							</tr>
						<table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>