<?php $__env->startSection('content'); ?>
<script type="text/javascript">
  function ConfirmDelete()
    {
      var x = confirm("Are you sure you want to delete ?");
      if(x)
        return true;
      else
        return false;
    }   
</script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo e(url('/awal')); ?>">Beranda</a>
        </li>
        <li class="breadcrumb-item active">Manajemen User Pasien</li>
      </ol>
			<?php if(session('status')): ?>
				<div class="alert alert-success">
					<strong><?php echo e(session('status')); ?></strong>
				</div>
			<?php endif; ?>
      <a class="nav-link"  href="<?php echo e(route ('pasien.add')); ?>"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data Pasien Baru</button></a>
      <hr>
      <div class="card mb-3">
      	<div class="card-header"><b>Manajemen User Pasien</b>
      	</div>
        <div class="card-body">
        	<div class="table-responsive">
        		<table class="table table-striped" width="100%" cellspacing="0">
        	    	<thead>
        	      		<tr>
											<th><font size="2px">No</font></th>
        	        		<th><font size="2px">Nama Pasien</font></th>
        	        		<th><font size="2px">Tempat Lahir</font></th>
        	        		<th><font size="2px">Tanggal Lahir</font></th>
        	        		<th><font size="2px">Umur</font></th>
        	        		<th><font size="2px">Gol. Darah</font></th>
        	        		<th><font size="2px">Alamat</font></th>
                      <th><font size="2px">No. Identitas</font></th>
        	        		<th><font size="2px">No. Telepon</font></th>
        	        		<th><font size="2px"><center>Aksi</center></font></th>
        	      		</tr>
        	    	</thead>
        	    	<tbody>
								<?php $no = 1; ?>
								<?php $__currentLoopData = $pasien; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $show): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        				<tr>
        					<td><font size="2px"><?php echo e($no++); ?></font></td>
        					<td><font size="2px"><?php echo e($show->nama_pasien); ?></font></td>
        					<td><font size="2px"><?php echo e($show->tmpt_lahir); ?></font></td>
        					<td><font size="2px"><?php echo e($show->tgl_lahir); ?></font></td>
        					<td><font size="2px"><?php echo e($show->umur_pasien); ?></font></td>
        					<td><font size="2px"><?php echo e($show->gol_darah); ?></font></td>
        					<td><font size="2px"><?php echo e($show->alamat); ?></font></td>
                  <td><font size="2px"><?php echo e($show->no_identitas); ?></font></td>
        					<td><font size="2px"></font></td>
        					<td>        						
										<center>
											<a data-toggle="tooltip" data-placement="top" title="LIhat Data" href="<?php echo e(url('/pasien/show')); ?>/<?php echo e($show->id); ?>">
                        <i class="fa fa-eye"></i>
        							</a>
											<a data-toggle="tooltip" data-placement="top" title="Edit" href="<?php echo e(url('/pasien/edit')); ?>/<?php echo e($show->id); ?>">
												<i class="fa fa-edit"></i>
        							</a>
											<a type="button" onclick="ConfirmDelete()" data-toggle="tooltip" data-placement="top" title="Hapus" href="<?php echo e(url('/pasien/delete')); ?>/<?php echo e($show->id); ?>">
                        <i class="fa fa-trash"></i>
											</a>
										</center>
        					</td>
        				</tr>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</tbody>
        	  	</table>
        	 </div>
        </div>
      </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>