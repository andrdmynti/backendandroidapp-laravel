<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="<?php echo e(url('/awal')); ?>">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="<?php echo e(url('/admin')); ?>">Manajemen Admin</a></li>
		  <li class="breadcrumb-item active">Detail Admin</li>
		</ol>
		<div class="card mb-3">
			<div class="card-header">
				<b>Detail Admin</b>
			</div>
			<div class="card-body">
				<div class="col-sm-12">		
					<form class="form-horizontal">
						<?php echo e(csrf_field()); ?>

						<input type="hidden" name="id" value="<?php echo e($admin->id); ?>">
						<table class="table table-bordered">
							<tr>
								<td class="col-sm-4">Nama</td>
								<td class="col-sm-8"><?php echo e($admin->name); ?></td>
							</tr>
							<tr>
								<td class="col-sm-4">Username</td>
								<td class="col-sm-8"><?php echo e($admin->username); ?></td>
							</tr>
							<tr>
								<td class="col-sm-4">Email</td>
								<td class="col-sm-8"><?php echo e($admin->email); ?></td>
							</tr>
							<tr>
								<td class="col-sm-4">Level</td>
								<td class="col-sm-8">
									<?php if($admin->level == 1): ?>
	                            		Admin
	                          		<?php endif; ?>
								</td>
							</tr>
						<table>
					</form>
				</div>	
			</div>
		</div>
		<br>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>