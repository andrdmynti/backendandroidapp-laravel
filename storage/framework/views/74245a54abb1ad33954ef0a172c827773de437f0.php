<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo e(url('/awal')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Management Content</li>
      </ol>
      <br>
      <a class="nav-link" data-toggle="modal" data-target="#ModalInsertContent"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Insert Content</button></a>
      <br><br>
      <div class="card mb-3">
      	<div class="card-header">
          	<i class="fa fa-sticky-note"></i> Management Content
      	</div>
        <div class="card-body">
        	<div class="table-responsive">
        		<table class="table table-striped" width="100%" cellspacing="0">
        	    	
        	    	<thead>
        	    		<?php  $no = 1;  ?>
        	    		<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        	      		<tr>
        	        		<th>No</th>
        	        		<th>Title</th>
        	        		<th colspan="3">News</th>
        	        		<th>Picture</th>
        	        		<th>Published Date</th>
        	        		<th><center>Action</center></th>
        	      		</tr>
        	    	</thead>
        	    	<tbody>
        				<tr>
        					<td><?php echo e($no++); ?></td>
        					<td><?php echo e($datas->title); ?></td>
        					<td colspan="3"><?php echo e($datas->news); ?></td>
        					<td>Gambar</td>
        					<td>22 March 2018</td>
        					<td>
        						<center>
        							<button type="button" class="btn btn-success btn-sm"><i class="fa fa-eye"></i>x Show</button>
        							<button type="button" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> Edit</button>
        							<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</button>
        						</center>
        					</td>
        				</tr>
        			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    	
        	    	</tbody>
        	  	</table>
        	 </div>
        </div>
      </div>
    </div>
</div>

<!-- Modal Insert-->
<div id="ModalInsertContent" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
      <form action="<?php echo e(route ('')); ?>">  
    	 <!-- Modal content-->
    	 <div class="modal-content">
      		<div class="modal-header">
        		<h4 class="modal-title" align="center">Insert Content</h4>
      			<button type="button" class="close" data-dismiss="modal">&times;</button>
      		</div>
      		<div class="modal-body">
      			<form action="" class="form-horizontal" method="POST" enctype="multipart/form-data">
      				<div class="form-group">
      					<label class="control-label">Title :</label>
      				    <div>
      				        <input type="text" class="form-control" name="judul" placeholder="Judul">
      				    </div>
      				</div>
      				<div class="form-group">
      				    <label class="control-label">News :</label>
      				    <div>
      				        <textarea class="form-control" name="berita" placeholder="News"></textarea>
      				    </div>
      				</div>
      				<div class="form-group">
      				    <label class="control-label">Picture :</label>
      				    <div>
      				        <input class="form-control" type="file" name="fileToUpload" id="fileToUpload">
      				    </div>
      				</div>
      				<div class="form-group">
      				    <label class="control-label">Published Date :</label>
      				    <div>
      				        <input type="text" class="form-control" name="berita" value="<?php date_default_timezone_get('Asia/Jakarta');
      				        	echo date('N F Y');?>" disabled>
      				    </div>
      				</div>
      				<br>
      				<div class="form-group">
      				    <div class="col-sm-12" align="right">
      				        <button class="btn btn-danger" type="submit">Simpan</button>
      				    </div>
      				</div>
      			</form>
      		</div>
      		<div class="modal-footer">
        		
      		</div>
        </div>
      </form>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>