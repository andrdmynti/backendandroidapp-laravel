<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="<?php echo e(url('/awal')); ?>">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="<?php echo e(url('/bidan')); ?>">Manajemen Bidan</a></li>
		  <li class="breadcrumb-item active"><i>Tambah Bidan</i></li>
		</ol>
		<div class="card mb-3">
      		<div class="card-header">
      			<b>Tambah Bidan</b>
      		</div>
		    <div class="card-body">
		<br>
		<div class="col-sm-12">		
			<form action="<?php echo e(route('bidan.insert')); ?>" class="form-horizontal" method="POST">
			<?php echo e(csrf_field()); ?>

			<div class="row">
					    <div class="col-sm-2">NIB</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="text" class="form-control" name="nib" placeholder="Nomor Induk Bidan" require></div>
			</div>
			<br>
			<div class="row">
					    <div class="col-sm-2">Nama</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="text" class="form-control" name="nama_bidan" placeholder="Nama Lengkap Bidan" require></div>
			</div>
			<br>
			<div class="row">
					    <div class="col-sm-2">Tempat Lahir</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="text" class="form-control" name="tempat" placeholder="Tempat Lahir Bidan" require></div>
			</div>
			<br>
			<div class="row">
					    <div class="col-sm-2">Tanggal Lahir</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="date" class="form-control" name="tgl_lahir" `require></div>
			</div>
			<br>
			<div class="row">
					    <div class="col-sm-2">Alamat</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><textarea class="form-control" name="alamat" placeholder="Alamat Lengkap Bidan" require></textarea></div>
			</div>
			<br>
			<div class="row">
					    <div class="col-sm-2">Tanggal Masuk</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="date" class="form-control" name="" placeholder="Tanggal Masuk Bidan" require></div>
			</div>
			<br>
				<div class="form-group">
	    			<p align="right">
	        			<button class="btn btn-danger" type="submit">Simpan</button>
	    			</p>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>