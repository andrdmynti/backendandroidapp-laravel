<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="<?php echo e(url('/awal')); ?>">Berita</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="<?php echo e(url('/admin')); ?>">Manajemen Data Pasien</a></li>
		  <li class="breadcrumb-item active"><i>Edit Data Pasien</i></li>
		</ol>
		<div class="card mb-3">
      		<div class="card-header">
      			<b>Edit Konten</b>
      		</div>
		    <div class="card-body">
			<br>
		<div class="col-sm-12">		
			<form action="<?php echo e(url('/pasien/update')); ?>/<?php echo e($pasien->id); ?>" class="form-horizontal" method="POST">
			<?php echo e(csrf_field()); ?>


			<input type="hidden" name="id" value="<?php echo e($pasien->id); ?>">
				<div class="row">
					    <div class="col-sm-3">Nama Pasien</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="nama_pasien" value="<?php echo e($pasien->nama_pasien); ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">Tempat Lahir Pasien</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="tmpt_lahir" value="<?php echo e($pasien->tmpt_lahir); ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">Tanggal Lahir Pasien</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="date" class="form-control" name="tgl_lahir" value="<?php echo e($pasien->tgl_lahir); ?>"></div>
				</div>
				<br>
            	<div class="row">
					    <div class="col-sm-3">Umur Pasien</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="number" class="form-control" name="umur_pasien" value="<?php echo e($pasien->umur_pasien); ?>"></div>
				</div>
				<br>
             	<div class="row">
					    <div class="col-sm-3">Golongan Darah Pasien</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="gol_darah" value="<?php echo e($pasien->gol_darah); ?>"></div>
				</div>
				<br> 
               <div class="row">
					    <div class="col-sm-3">Alamat Pasien</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="alamat" value="<?php echo e($pasien->alamat); ?>"></div>
				</div>
				<br>
               	<div class="row">
					    <div class="col-sm-3">No. Identitas Pasien</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="number" class="form-control" name="no_identitas" value="<?php echo e($pasien->no_identitas); ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-3">No. Telepon Wali</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-8"><input type="text" class="form-control" name="" value=""></div>
				</div>
				<br>
				<div class="form-group">
	    			<p align="right">
	        			<button class="btn btn-danger" type="submit">Update</button>
	    			</p>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>