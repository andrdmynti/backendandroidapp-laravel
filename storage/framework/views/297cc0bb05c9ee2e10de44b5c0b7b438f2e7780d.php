<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="<?php echo e(url('/awal')); ?>">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="<?php echo e(url('/pasien')); ?>">Manajemen Pasien</a></li>
		  <li class="breadcrumb-item active">Tambah Data Pasien Baru</li>
		</ol>
		<div class="card mb-3">
			<div class="card-header">
				<b>Tambah Data Baru Pasien</b>
			</div>
			<div class="card-body">
				<br>
				<div class="col-sm-12">		
					<form action="<?php echo e(route('pasien.insert')); ?>" class="form-horizontal" method="POST">
					<?php echo e(csrf_field()); ?>

						<div class="row">
							<div class="col-sm-2">Nama</div>
							<div class="col-sm-1">:</div>
			    			<div class="col-sm-9">
			        			<input type="text" class="form-control" name="nama_pasien" placeholder="Nama Lengkap Pasien" required>
			    			</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-2">Tempat Lahir</div>
							<div class="col-sm-1">:</div>
			    			<div class="col-sm-9">
			        			<input class="form-control" name="tmpt_lahir" placeholder="Tempat Lahir Pasien" require>
			    			</div>
						</div>
						<br>
						<div class="row">
			    			<div class="col-sm-2">Tanggal Lahir</div>
			    			<div class="col-sm-1">:</div>
			    			<div class="col-sm-9">
			        			<input type="date" class="form-control" name="tgl_lahir" require>
			    			</div>
						</div>
						<br>
						<div class="row">
							<div class="col-sm-2">Umur Pasien</div>
							<div class="col-sm-1">:</div>
							<div class="col-sm-9">
								<input type="number" min="1" class="form-control" name="umur_pasien" value="1" required>
							</div>
						</div>
						<br>
						<div class="row">
			    			<div class="col-sm-2">Golongan Darah</div>
			    			<div class="col-sm-1">:</div>
			    			<div class="col-sm-9">
			        			<select class="form-control" name="gol_darah">
			        				<option>--Pilih Golongan Darah--</option>
									<option value="A">A</option>
									<option value="B">B</option>
									<option value="O">O</option>
									<option value="AB">AB</option>
								</select>
			    			</div>
						</div>
						<br>
						<div class="row">
			    			<div class="col-sm-2">Alamat</div>
			    			<div class="col-sm-1">:</div>
			    			<div class="col-sm-9">
			        			<textarea class="form-control" name="alamat" placeholder="Alamat Pasien" require></textarea>
			    			</div>
						</div>
						<br>
						<div class="row">
			    			<div class="col-sm-2">No Identitas</div>
			    			<div class="col-sm-1">:</div>
			    			<div class="col-sm-9">
			        			<input type="text" class="form-control" name="no_identitas" placeholder="Nomor Induk Kependudukan" require>
			    			</div>
						</div>
						<br>
						<br>
						<div class="row">
			    			<div class="col-sm-2">No Telepon</div>
			    			<div class="col-sm-1">:</div>
			    			<div class="col-sm-9">
			        			<input type="text" class="form-control" name="" placeholder="Nomor Telepon" require>
			    			</div>
						</div>
						<br>
						<div class="form-group">
			    			<p align="right">
			        			<button class="btn btn-primary" type="submit">Simpan</button>
			        			<a href="<?php echo e(route('pasien.list')); ?>"><button class="btn btn-danger" type="button">Batal</button></a>
			    			</p>
						</div>
					</form>
				</div>	
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>