 

<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="<?php echo e(url('/awal')); ?>">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="<?php echo e(url('/bidan')); ?>">Manajemen Bidan</a></li>
		  <li class="breadcrumb-item active"><i>Edit Bidan</i></li>
		</ol>
		<div class="card mb-3">
      		<div class="card-header">
      			<b>Edit Bidan</b>
      		</div>
		    <div class="card-body">
			<br>
		<div class="col-sm-12">		
			<form action="<?php echo e(url('/bidan/update')); ?>/<?php echo e($bidan->id); ?>" class="form-horizontal" method="POST">
			<?php echo e(csrf_field()); ?>


			<input type="hidden" name="id" value="<?php echo e($bidan->id); ?>">
				<div class="row">
					    <div class="col-sm-2">NIB</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="text" class="form-control" name="nib" value="<?php echo e($bidan->nib); ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-2">Nama</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="text" class="form-control" name="nama_bidan" value="<?php echo e($bidan->nama_bidan); ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-2">Tempat Lahir</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="text" class="form-control" name="tempat" value="<?php echo e($bidan->tempat_lahir); ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-2">Tanggal Lahir</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="date" class="form-control" name="tgl_lahir" value="<?php echo e($bidan->tgl_lahir); ?>"></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-2">Alamat</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><textarea type="text" class="form-control" name="alamat"><?php echo e($bidan->alamat); ?></textarea></div>
				</div>
				<br>
				<div class="row">
					    <div class="col-sm-2">Tanggal Masuk</div>
					    <div class="col-sm-1">:</div>
					    <div class="col-sm-9"><input type="date" class="form-control" name="alamat"></div>
				</div>
				<br>
				<div class="form-group">
	    			<div class="col-sm-12" align="right">
	        			<button class="btn btn-danger" type="submit">Edit</button>
	    			</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>