<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo e(url('/awal')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Management Users Admin</li>
      </ol>
      <br>
      <a class="nav-link" data-toggle="modal" data-target="#exampleModal"><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Insert Konten</button>
      <br><br>
      <div class="card mb-3">
      	<div class="card-header">
          	<i class="fa fa-sticky-note"></i> Management Users Admin
      	</div>
        <div class="card-body">
        	<div class="table-responsive">
        		<table class="table table-striped" width="100%" cellspacing="0">
        	    	<thead>
        	      		<tr>
        	        		<th>Name</th>
        	        		<th>Position</th>
        	        		<th colspan="3">Office</th>
        	        		<th>Date of Publish</th>
        	        		<th><center>Action</center></th>
        	      		</tr>
        	    	</thead>
        	    	<tbody>
        				<tr>
        					<td>Tiger Nixon</td>
        					<td>System Architect</td>
        					<td>Edinburgh</td>
        					<td>Edinburgh</td>
        					<td>Edinburgh</td>
        					<td>22 March 2018</td>
        					<td>
        						<center>
        							<button type="button" class="btn btn-success btn-sm"><i class="fa fa-eye"></i> Show</button>
        							<button type="button" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> Edit</button>
        							<button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</button>
        						</center>
        					</td>
        				</tr>    	
        	    	</tbody>
        	  	</table>
        	 </div>
        </div>
      </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>