<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="<?php echo e(url('/awal')); ?>">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="<?php echo e(url('/pasien')); ?>">Manajemen Pasien</a></li>
		  <li class="breadcrumb-item active"><i>Detail Data Pasien</i></li>
		</ol>
		<div class="card mb-3">
			<div class="card-header">
				<b>Detail Data Pasien</b>
			</div>
			<div class="card body">
				<div class="col-sm-12">		
					<form class="form-horizontal">
						<?php echo e(csrf_field()); ?>

						<input type="hidden" name="id" value="<?php echo e($pasien->id); ?>">
						<br>
						<table class="table table-bordered">
							<tr>
								<td class="col-sm-2">Nama Pasien</td>
								<td class="col-sm-9"><?php echo e($pasien->nama_pasien); ?></td>
							</tr>
							<tr>
								<td class="col-sm-2">Tempat Lahir Pasien</td>
								<td class="col-sm-9"><?php echo e($pasien->tmpt_lahir); ?></td>
							</tr>
							<tr>
								<td class="col-sm-2">Tanggal Lahir Pasien</td>
								<td class="col-sm-9"><?php echo e($pasien->tgl_lahir); ?></td>
							</tr>
							<tr>
								<td class="col-sm-2">Umur Pasien</td>
								<td class="col-sm-9"><?php echo e($pasien->umur_pasien); ?></td>
							</tr>
							<tr>
								<td class="col-sm-3">Golongan Darah Pasien</td>
								<td class="col-sm-9"><?php echo e($pasien->gol_darah); ?></td>
							</tr>
							<tr>
								<td class="col-sm-2">Alamat Pasien</td>
								<td class="col-sm-9"><?php echo e($pasien->alamat); ?></td>
							</tr>
							<tr>
								<td class="col-sm-2">No. Identitas Pasien</td>
								<td class="col-sm-9"><?php echo e($pasien->no_identitas); ?></td>
							</tr>
							<tr>
								<td class="col-sm-2">No. Telepon</td>
								<td class="col-sm-9"></td>
							</tr>
						<table>
					</form>
				</div>	
			</div>
		</div>
		<br>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>