<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="<?php echo e(url('/awal')); ?>">Beranda &nbsp;</a>/
		  </libreadcrumb>
		  <li class="-item active"><a href="<?php echo e(url('/content')); ?>">&nbsp;&nbsp;Manajemen Konten &nbsp; </a>/</li>
		  <li class="breadcrumb-item active"><i>&nbsp;&nbsp;Edit Konten</i></li>
		</ol>
		<div class="card mb-3">
      		<div class="card-header">
      			<b>Edit Konten</b>
      		</div>
		    <div class="card-body">
			<br>
			<div class="col-sm-12">		
				<form action="<?php echo e(url('/content/update')); ?>/<?php echo e($content->id); ?>" class="form-horizontal" method="POST" enctype="multipart/form-data">
				<?php echo e(csrf_field()); ?>


				<input type="hidden" name="id" value="<?php echo e($content->id); ?>">
					<div class="row">
						    <div class="col-sm-2">Judul</div>
						    <div class="col-sm-1">:</div>
						    <div class="col-sm-9"><input type="text" class="form-control" name="title" value="<?php echo e($content->title); ?>" placeholder="Judul" required></div>
					</div>
					<br>
					<div class="row">
						    <div class="col-sm-2">Berita</div>
						    <div class="col-sm-1">:</div>
						    <div class="col-sm-9"><textarea class="form-control" name="news"><?php echo e($content->news); ?></textarea></div>
					</div>
					<br>
					<div class="row">
						    <div class="col-sm-2">Gambar</div>
						    <div class="col-sm-1">:</div>
						    <div class="col-sm-9">
						    	<img width="180" height="100" src="<?php echo e(url( 'storage/app/images/'.$content->picture)); ?>">
						    </div>
						    <!-- <div class="col-sm-10"><input class="form-control" type="file" name="fileToUpload" id="fileToUpload" value="<?php echo e($content->picture); ?>"></div> -->
					</div>
					<br>
					<div class="form-group">
		    			<p align="right">
		        			<button class="btn btn-primary" type="submit">Simpan</button>
		    			</p>
					</div>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>"
<?php echo $__env->make('layouts/layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>