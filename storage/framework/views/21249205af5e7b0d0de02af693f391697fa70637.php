<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo e(url('/awal')); ?>">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Management Users Pasien</li>
      </ol>
      <br>
      <a><button type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Insert Konten</button></a>
      <br><br>
      <div class="card mb-3">
      	<div class="card-header">
          	<i class="fa fa-sticky-note"></i> Management Users Pasien
      	</div>
        <div class="card-body">
        	<div class="table-responsive">
        		<table class="table table-striped" width="100%" cellspacing="0">
        	    	<thead>
        	      		<tr>
											<th>No</th>
        	        		<th>Nama Pasien</th>
        	        		<th>Tempat,Tanggal Lahir</th>
        	        		<th>Umur</th>
        	        		<th>Gol. Darah</th>
        	        		<th>Alamat</th>
        	        		<th>No. Identitas</th>
        	        		<th><center>Action</center></th>
        	      		</tr>
        	    	</thead>
        	    	<tbody>
								<?php $no = 1; ?>
								<?php $__currentLoopData = $pasien; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $show): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        				<tr>
        					<td><?php echo e($no++); ?></td>
        					<td><?php echo e($show->nama_pasien); ?></td>
        					<td><?php echo e($show->ttl_pasien); ?></td>
        					<td><?php echo e($show->umur_pasien); ?></td>
        					<td><?php echo e($show->gol_darah); ?></td>
        					<td><?php echo e($show->alamat); ?></td>
        					<td><?php echo e($show->no_identitas); ?></td>
        					<td>        						
										<!-- <center>
											<a href="<?php echo e(url('/user/show')); ?>/<?php echo e($show->id); ?>">
												<button type="button" class="btn btn-success btn-sm"><i class="fa fa-eye"></i> Show</button>
        							</a>
											<a href="<?php echo e(url('/user/edit')); ?>/<?php echo e($show->id); ?>">
												<button type="button" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> Edit</button>
        							</a>
											<a href="<?php echo e(url('/admin/delete')); ?>/<?php echo e($show->id); ?>">
												<button type="button" class="btn btn-danger btn-sm" onclick="ConfirmDelete()"><i class="fa fa-trash"></i> Delete</button>
											</a>
										</center> -->
        					</td>
        				</tr>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</tbody>
        	  	</table>
        	 </div>
        </div>
      </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>