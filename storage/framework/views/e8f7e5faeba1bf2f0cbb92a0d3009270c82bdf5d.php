<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<ol class="breadcrumb">
		  <li class="breadcrumb-item">
		    <a href="<?php echo e(url('/awal')); ?>">Beranda</a>
		  </li>
		  <li class="breadcrumb-item active"><a href="<?php echo e(url('/bidan')); ?>">Manajemen Bidan</a></li>
		  <li class="breadcrumb-item active">Detail Bidan</li>
		</ol>
		<div class="card mb-3">
			<div class="card-header">
				<b>Detail Bidan</b>
			</div>
			<div class="card-body">
				<div class="col-sm-12">		
					<form class="form-horizontal">
						<?php echo e(csrf_field()); ?>

						<input type="hidden" name="id" value="<?php echo e($bidan->bidan_id); ?>">
						<table class="table table-bordered">
							<tr>
								<td class="col-sm-2">NIB</td>
								<td class="col-sm-10"><?php echo e($bidan->nib); ?></td>
							</tr>
							<tr>
								<td class="col-sm-2">Nama</td>
								<td class="col-sm-10"><?php echo e($bidan->nama_bidan); ?></td>
							</tr>
							<tr>
								<td class="col-sm-2">Tempat Lahir</td>
								<td class="col-sm-10"><?php echo e($bidan->tempat_lahir); ?></td>
							</tr>
							<tr>
								<td class="col-sm-2">Tanggal Lahir</td>
								<td class="col-sm-10"><?php echo e($bidan->tgl_lahir); ?></td>
							</tr>
							<tr>
								<td class="col-sm-2">Alamat</td>
								<td class="col-sm-10"><?php echo e($bidan->alamat); ?></td>
							</tr>
							<tr>
								<td class="col-sm-2">Tanggal Masuk</td>
								<td class="col-sm-10"></td>
							</tr>
						<table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>