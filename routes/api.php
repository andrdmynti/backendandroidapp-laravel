<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::prefix('pasien')->group(function() {
	Route::post('list', 'Pasien\Service\PasienService@getPasien');
	Route::post('create', 'Pasien\Service\PasienService@postPasienAdd');
	Route::post('update', 'Pasien\Service\PasienService@postPasienEdit');
});

Route::prefix('bidan')->group(function() {
	Route::post('list', 'Bidan\Service\BidanService@getBidan');
	Route::post('create', 'Bidan\Service\BidanService@postBidanAdd');
	Route::post('update', 'Bidan\Service\BidanService@postBidanEdit');
});

Route::prefix('content')->group(function() {
	Route::post('list', 'Content\Service\ContentService@getContent');
});

Route::prefix('redis')->group(function() {
	Route::post('list', 'Redis\Service\RedisService@getRedis');
	Route::post('create', 'Redis\Service\RedisService@postRedisAdd');
	Route::post('update', 'Redis\Service\RedisService@postRedisEdit');
});