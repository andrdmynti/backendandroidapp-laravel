<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//tampilan login


//login
Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::group(['namespace' => 'Auth' ,'as' => 'auth.'], function() {
	Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('login');
    // Route::post('register', 'RegisterController@register')->name('register');
    Route::post('logout', 'LoginController@logout')->name('logout');   
});     

//route untuk menampilkan dashboard
Route::get('awal', function () {
    return view('index');
});

//route untuk menampilkan help
Route::get('help', function () {
    return view('help.help');
});
Route::get('konten', function () {
    return view('help.konten');
});
Route::get('remedis', function () {
    return view('help.redis');
});
Route::get('adminhelp', function () {
    return view('help.admin');
});
Route::get('bidanhelp', function () {
    return view('help.bidan');
});
Route::get('pasienhelp', function () {
    return view('help.pasien');
});

//route content
 Route::get('content', 'Content\Controller\ContentController@index')->name('content.list');
 Route::get('content/add', 'Content\Controller\ContentController@create')->name('content.add');
 Route::post('content/create', 'Content\Controller\ContentController@store')->name('content.insert');
 Route::get('content/show/{id}', 'Content\Controller\ContentController@show')->name('content.show');
 Route::get('content/edit/{id}', 'Content\Controller\ContentController@edit')->name('content.edit');
 Route::post('content/update/{id}', 'Content\Controller\ContentController@update')->name('content.update');
 Route::get('content/delete/{id}', 'Content\Controller\ContentController@destroy')->name('content.delete');
 
//route CRUD admin
Route::get('admin', 'Admin\Controller\AdminController@index')->name('admin.list');
Route::get('admin/add', 'Admin\Controller\AdminController@create')->name('admin.add');
Route::post('admin/create', 'Admin\Controller\AdminController@store')->name('admin.insert');
Route::get('admin/show/{id}', 'Admin\Controller\AdminController@show')->name('admin.show');
Route::get('admin/edit/{id}', 'Admin\Controller\AdminController@edit')->name('admin.edit');
Route::post('admin/update/{id}', 'Admin\Controller\AdminController@update')->name('admin.update');
Route::get('admin/delete/{id}', 'Admin\Controller\AdminController@destroy')->name('admin.delete');

//route CRUD admin
Route::get('bidan', 'Bidan\Controller\BidanController@index')->name('bidan.list');
Route::get('bidan/add', 'Bidan\Controller\BidanController@create')->name('bidan.add');
Route::post('bidan/create', 'Bidan\Controller\BidanController@store')->name('bidan.insert');
Route::get('bidan/show/{id}', 'Bidan\Controller\BidanController@show')->name('bidan.show');
Route::get('bidan/edit/{id}', 'Bidan\Controller\BidanController@edit')->name('bidan.edit');
Route::post('bidan/update/{id}', 'Bidan\Controller\BidanController@update')->name('bidan.update');
Route::get('bidan/delete/{id}', 'Bidan\Controller\BidanController@destroy')->name('bidan.delete');

//route CRUD pasien
Route::get('pasien', 'Pasien\Controller\PasienController@index')->name('pasien.list');
Route::get('pasien/add', 'Pasien\Controller\PasienController@create')->name('pasien.add');
Route::post('pasien/create', 'Pasien\Controller\PasienController@store')->name('pasien.insert');
Route::get('pasien/show/{id}', 'Pasien\Controller\PasienController@show')->name('pasien.show');
Route::get('pasien/edit/{id}', 'Pasien\Controller\PasienController@edit')->name('pasien.edit');
Route::post('pasien/update/{id}', 'Pasien\Controller\PasienController@update')->name('pasien.update');
Route::get('pasien/delete/{id}', 'Pasien\Controller\PasienController@destroy')->name('pasien.delete');

//route CRUD redis pasien
Route::get('redis', 'Redis\Controller\RedisController@index')->name('redis.list');
Route::get('redis/add', 'Redis\Controller\RedisController@create')->name('redis.add');
Route::post('redis/create', 'Redis\Controller\RedisController@store')->name('redis.insert');
Route::get('redis/show/{id}', 'Redis\Controller\RedisController@show')->name('redis.show');
Route::get('redis/edit/{id}', 'Redis\Controller\RedisController@edit')->name('redis.edit');
Route::post('redis/update/{id}', 'Redis\Controller\RedisController@update')->name('redis.update');
Route::get('redis/delete/{id}', 'Redis\Controller\RedisController@destroy')->name('redis.delete');

//route CRUD data training
Route::get('training', 'Training\Controller\TrainingController@index')->name('training.list');
Route::get('training/add', 'Training\Controller\TrainingController@create')->name('training.add');
Route::post('training/create', 'Training\Controller\TrainingController@store')->name('training.insert');
Route::get('training/show/{id}', 'Training\Controller\TrainingController@show')->name('training.show');
Route::get('training/edit/{id}', 'Training\Controller\TrainingController@edit')->name('training.edit');
Route::post('training/update/{id}', 'Training\Controller\TrainingController@update')->name('training.update');
Route::get('training/delete/{id}', 'Training\Controller\TrainingController@destroy')->name('training.delete');