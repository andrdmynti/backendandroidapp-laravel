<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekamedisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekamedis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pasien_id')->unsigned();
            $table->foreign('pasien_id')->references('id')->on('pasien');
            $table->integer('bidan_id')->unsigned();
            $table->foreign('bidan_id')->references('id')->on('bidan');
            $table->text('sistol');
            $table->text('diastol');
            $table->integer('minggu_ke');
            $table->integer('penyakit_menular');
            $table->integer('mata_minus');
            $table->integer('asma');
            $table->integer('jantung');
            $table->integer('hipertensi');
            $table->integer('diabetes');
            $table->integer('sesar');
            $table->integer('pinggul');
            $table->integer('p_previa');
            $table->integer('b_sungsang');
            $table->integer('b_kembar');
            $table->integer('b_jantung_lemah');
            $table->integer('fetal_distress');
            $table->integer('b_giant');
            $table->integer('percentage_sesar');
            $table->integer('percentage_normal');
            $table->integer('hasil');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekamedis');
    }
}
