<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasienTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasien', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_pasien');
            $table->string('tmpt_lahir');
            $table->string('tgl_lahir');
            $table->integer('umur_pasien');
            $table->string('gol_darah');
            $table->text('alamat');
            $table->string('no_identitas');
            $table->string('nama_wali');
            $table->string('nohp_wali');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasien');
    }
}
