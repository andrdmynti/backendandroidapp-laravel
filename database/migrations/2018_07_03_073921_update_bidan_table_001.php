<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBidanTable001 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bidan', function (Blueprint $table) {
                $table->integer('nib')->after('id');
                $table->string('tempat_lahir')->after('nama_bidan');
                $table->string('tgl_lahir')->after('tempat_lahir');
                $table->string('alamat')->after('tgl_lahir');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
