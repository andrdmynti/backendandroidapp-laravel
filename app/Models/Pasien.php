<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    protected $table = 'pasien';

    protected $fillable = [
    	'nama_pasien', 'tmpt_lahir', 'tgl_lahir', 'umur_pasien', 'gol_darah', 'alamat', 'no_identitas', 'nama_wali', 'nohp_wali'
    ];
}
