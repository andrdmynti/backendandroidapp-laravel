<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bidan extends Model
{
    protected $table = 'bidan';

    protected $fillable = [
    	'nib', 'nama_bidan', 'tempat_lahir', 'tgl_lahir', 'alamat', 'status', 'created_at'
    ];

}
