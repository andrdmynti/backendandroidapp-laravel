<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Redis extends Model
{
    protected $table = 'rekamedis';

    protected $fillable = [
    	'bidan_id', 'pasien_id', 'minggu_ke', 'sistol', 'diastol', 'penyakit_menular', 'mata_minus', 'asma', 'jantung', 'hipertensi', 'diabetes', 'sesar', 'pinggul', 'p_previa', 'b_sungsang', 'b_kembar', 'b_jantung_lemah', 'fetal_distress', 'b_giant'
    ];

    public function pasien(){
    	return $this->belongsTo('\App\Models\Pasien','pasien_id','id');
    }

    public function bidan(){
    	return $this->belongsTo('\App\Models\Bidan','bidan_id','id');
    }

    // belongsTo --> One to Many (refresh)
    // hashMany -- > One to Many / Hash Many
    // hashOne  -- > One to One / Many to Many


}
