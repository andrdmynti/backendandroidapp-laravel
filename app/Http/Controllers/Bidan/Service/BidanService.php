<?php

namespace App\Http\Controllers\Bidan\Service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bidan;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class BidanService extends Controller
{
    public function getBidan()
    {

    	$query = Bidan::all();
    	if(count($query) > 0)
    	{
    		$result = array('result' => 1, 'data' => $query);
    	}
    	else
    	{
    		$result = array('result' => 0, 'data' => 'null');	
    	}

    	return $result;

    }


    public function postBidanAdd()
    {

    	$input = Input::all();

    	$Validator = Validator::make(Input::all(),[
    		'nib' 			=> 'required',
    		'nama_bidan' 	=> 'required',
    		'tempat_lahir' 	=> 'required',
    		'tgl_lahir' 	=> 'required',
            'alamat'        => 'required',
    		'status' 		=> 'required'
    	]);

    	if($Validator->fails())
    	{
    		$result = array("result" => 0, "error" => 'Data Not Complete', "errorField" => $Validator->errors());
    	}
    	else
    	{
    		try {

    			  $data  = Bidan::create($input);
    			  if (count($data) > 0)
    			  {
    			  	  $result = array("result" => 1, "message" => 'Data Succsessful Entry', "data" => $data);
    			  }
    			  else
    			  {
    			  	  $result = array("result" => 0, "message" => 'Data Failed Entry', "data" => 'null'); 
    			  }
    			
    		} catch (Exception $e) {

    			$result = array("result" => 0, "message" => 'Data Failed Entry', "data" => $e->getMessage()." ".$e->getFile()." ".$e->getLine()); 
    			
    		}
    	
    	}

      return $result;

    }

    public function postBidanEdit()
    {

    	$input = Input::all();

    	$Validator = Validator::make(Input::all(),[
    		'nib' 			=> 'required',
    		'nama_bidan' 	=> 'required',
    		'tempat_lahir' 	=> 'required',
    		'tgl_lahir' 	=> 'required',
            'alamat'        => 'required',
    		'status' 		=> 'required'
    	]);

    	if($Validator->fails())
    	{
    		$result = array("result" => 0, "error" => 'Data Not Complete', "errorField" => $Validator->errors());
    	}
    	else
    	{
    		try {

    			  $update  = Bidan::find($input['id']);
    			  $update->update($input);

    			  if (count($update) > 0)
    			  {
    			  	  $result = array("result" => 1, "message" => 'Data Succsessful Edited', "data" => $update); 
    			  }
    			  else
    			  {
    			  	  $result = array("result" => 1, "message" => 'Data Failed Entry', "data" => 'null'); 
    			  }
    			
    		} catch (Exception $e) {

    			$result = array("result" => 1, "message" => 'Data Failed Entry', "data" => $e->getMessage()." ".$e->getFile()." ".$e->getLine()); 
    			
    		}
    	
    	}

      return $result;

    }

}
