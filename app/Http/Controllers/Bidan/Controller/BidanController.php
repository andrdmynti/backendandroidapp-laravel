<?php

namespace App\Http\Controllers\Bidan\Controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bidan;

class BidanController extends Controller
{
    //
    public function __construct(){

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bidan = Bidan::all();
        return view('bidan.bidan', compact('bidan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bidan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insert = new Bidan;
            $insert->nib                  = $request->nib;
            $insert->nama_bidan           = $request->nama_bidan;
            $insert->tempat_lahir         = $request->tempat;
            $insert->tgl_lahir            = $request->tgl_lahir;
            $insert->alamat               = $request->alamat;
            $insert->status               = '1';
            $insert->save();

        return redirect('/bidan')->with('status', 'Content succsess');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bidan = Bidan::find($id);
        return view('bidan.show', compact('bidan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bidan = Bidan::find($id);
        return view('bidan.update', compact('bidan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit  = Bidan::find($request->id);
            $edit->nib                  = $request->nib;
            $edit->nama_bidan           = $request->nama_bidan;
            $edit->tempat_lahir         = $request->tempat;
            $edit->tgl_lahir            = $request->tgl_lahir;
            $edit->alamat               = $request->alamat;
        $edit->update();

        return redirect('/bidan')->with('status','Bidan Edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Bidan::where('id', $id);
        $delete->delete();

        return redirect('/bidan')->with('status', 'Data Bidan Dihapus');
    }
}