<?php

namespace App\Http\Controllers\Content\Service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ContentService extends Controller
{
    public function getContent()
    {

    	$query = Content::all();
    	if(count($query) > 0)
    	{
    		$result = array('result' => 1, 'data' => $query);
    	}
    	else
    	{
    		$result = array('result' => 0, 'data' => 'null');	
    	}

    	return $result;

    }

}