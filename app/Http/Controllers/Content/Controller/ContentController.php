<?php

namespace App\Http\Controllers\Content\Controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Content;

class ContentController extends Controller
{
    
    public function __construct(){

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $content = Content::all();
        return view('content.content', compact('content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('content.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        $add = new Content;
            $add->title     = $request->title;
            $add->news      = $request->news;
            
            //Berhasil but salah ngarahin
            // $file = $request->file('picture');
            //     $fileName   = $file->getClientOriginalExtension();
            //     $newName    = rand(100000,1001238912).".".$fileName;
            //     $request->file('picture')->move("storage/app/images", $newName);
            // $add->picture   = $file;

            //upload foto
            $img_name = uniqid().'.jpg';
                $request->file('picture')->storeAs('images/', $img_name);
            $add->picture = $img_name;
            //end upload foto
            $add->save();

         return redirect('/content')->with('status', 'Content Success');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $content = Content::find($id);
        return view('content.show', compact('content'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $content = Content::find($id);
        return view('content.update', compact('content'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit = Content::find($request->id);
            $edit->title     = $request->title;
            $edit->news      = $request->news;
        $edit->update();   
        
        return redirect('/content')->with('status', 'Content Edit');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $delete = Content::where('id', $id);
         $delete->delete();  

         return redirect('/content')->with('status', 'Content Delete');  
    }
}