<?php

namespace App\Http\Controllers\Admin\Controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin;

class AdminController extends Controller
{
    
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = Admin::where('level',1)->get();
        return view('admin.admin', compact('admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insert = new Admin;
            $insert->name           = $request->name;
            $insert->username       = $request->username;
            $insert->email          = $request->email;
            $insert->password       = bcrypt($request->password);
            $insert->level          = $request->level;
            $insert->save();

            return redirect('/admin')->with('status', 'Content succsess');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin  = Admin::find($id);
        return view('admin.show', compact('admin'));
    }
    
    // public function ajaxDetail(Request $request){
    //     $id         = $request->id;
    //     $show       = Admin::find($id);

    //         $response = [
    //             'id'            => $show->id, 
    //             'name'          => $show->name, 
    //             'username'      => $show->username, 
    //             'email'         => $show->email, 
    //             'password'      => $show->password
    //         ];
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::find($id);
        return view('admin.update', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit  = Admin::find($request->id);
            $edit->name         = $request->name;
            $edit->username     = $request->username;
            $edit->email        = $request->email;
        $edit->update();

        return redirect('/admin')->with('status','Admin Edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Admin::where('id', $id);
        $delete->delete();

        return redirect('/admin')->with('status', 'User Dihapus');
    }
}