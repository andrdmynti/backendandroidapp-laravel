<?php

namespace App\Http\Controllers\Pasien\Controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pasien;

class PasienController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pasien = Pasien::all();
        return view ('pasien.pasien', compact('pasien'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pasien.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $add = new Pasien;
            $add->nama_pasien   = $request->nama_pasien;
            $add->tmpt_lahir    = $request->tmpt_lahir;
            $add->tgl_lahir     = $request->tgl_lahir;
            $add->umur_pasien   = $request->umur_pasien;
            $add->gol_darah     = $request->gol_darah;
            $add->alamat        = $request->alamat;
            $add->no_identitas  = $request->no_identitas;
            $add->nama_wali     = $request->nama_wali;
            $add->nohp_wali     = $request->nohp_wali;
            $add->save();

         return redirect('/pasien')->with('status', 'Pasien succsess');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pasien = Pasien::find($id);
        return view ('pasien.show', compact('pasien'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pasien = Pasien::find($id);
        return view ('pasien.update', compact('pasien'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit = Pasien::find($request->id);
            $edit->nama_pasien  = $request->nama_pasien;
            $edit->tmpt_lahir   = $request->tmpt_lahir;
            $edit->tgl_lahir    = $request->tgl_lahir;
            $edit->umur_pasien  = $request->umur_pasien;
            $edit->gol_darah    = $request->gol_darah;
            $edit->alamat       = $request->alamat;
            $edit->no_identitas = $request->no_identitas;
            $edit->nama_wali    = $request->nama_wali;
            $edit->nohp_wali    = $request->nohp_wali;
        $edit->update();   
        
        return redirect('/pasien')->with('status', 'Data Pasien Edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Pasien::where('id', $id);
        $delete->delete();  

        return redirect('/pasien')->with('status', 'Data Pasien Delete');  
    }
}
