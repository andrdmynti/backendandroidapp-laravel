<?php

namespace App\Http\Controllers\Pasien\Service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pasien;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PasienService extends Controller
{
    
	public function getPasien()
	{

		$query = Pasien::all();
		if(count($query) > 0)
		{
			$result = array('result' => 1, 'data' => $query);
		}
		else
		{
			$result = array('result' => 0, 'data' => 'null');	
		}

		return $result;

	}


	public function postPasienAdd()
	{

		$input = Input::all();

		$Validator = Validator::make(Input::all(),[
			'nama_pasien' 	=> 'required',
			'tmpt_lahir' 	=> 'required',
			'tgl_lahir' 	=> 'required',
			'umur_pasien' 	=> 'required',
			'gol_darah'		=> 'required',
			'alamat'		=> 'required',
			'no_identitas' 	=> 'required',
			'nama_wali' 	=> 'required',
			'nohp_wali' 	=> 'required'
		]);

		if($Validator->fails())
		{
			$result = array("result" => 0, "error" => 'Data Not Complete', "errorField" => $Validator->errors());
		}
		else
		{
			try {

				  $data  = Pasien::create($input);
				  if (count($data) > 0)
				  {
				  	  $result = array("result" => 1, "message" => 'Data Succsessful Entry', "data" => $data);
				  }
				  else
				  {
				  	  $result = array("result" => 0, "message" => 'Data Failed Entry', "data" => 'null'); 
				  }
				
			} catch (Exception $e) {

				$result = array("result" => 0, "message" => 'Data Failed Entry', "data" => $e->getMessage()." ".$e->getFile()." ".$e->getLine()); 
				
			}
		
		}

	  return $result;

	}

	public function postPasienEdit()
    {

    	$input = Input::all();

    	$Validator = Validator::make(Input::all(),[
    		'nama_pasien' 	=> 'required',
			'tmpt_lahir' 	=> 'required',
			'tgl_lahir' 	=> 'required',
			'umur_pasien' 	=> 'required',
			'gol_darah'		=> 'required',
			'alamat'		=> 'required',
			'no_identitas' 	=> 'required',
			'nama_wali' 	=> 'required',
			'nohp_wali' 	=> 'required'
    	]);

    	if($Validator->fails())
    	{
    		$result = array("result" => 0, "error" => 'Data Not Complete', "errorField" => $Validator->errors());
    	}
    	else
    	{
    		try {

    			  $update  = Pasien::find($input['id']);
    			  $update->update($input);

    			  if (count($update) > 0)
    			  {
    			  	  $result = array("result" => 1, "message" => 'Data Succsessful Edited', "data" => $update); 
    			  }
    			  else
    			  {
    			  	  $result = array("result" => 1, "message" => 'Data Failed Entry', "data" => 'null'); 
    			  }
    			
    		} catch (Exception $e) {

    			$result = array("result" => 1, "message" => 'Data Failed Entry', "data" => $e->getMessage()." ".$e->getFile()." ".$e->getLine()); 
    			
    		}
    	
    	}

      return $result;

    }


}
