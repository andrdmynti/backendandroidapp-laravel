<?php

namespace App\Http\Controllers\Redis\Controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Redis;
use App\Models\Bidan;
use App\Models\Pasien;

class RedisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $redis = Redis::all();
        return view ('redis.redis', compact('redis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bidan = Bidan::all();
        $pasien = Pasien::all();
        return view ('redis.create', compact('bidan', 'pasien'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $add = new Redis;
            $add->nama_pasien       = $request->nama_pasien;
            $add->nama_bidan        = $request->nama_bidan;
            $add->sistol            = $request->sistol;
            $add->diastol           = $request->diastol;
            $add->minggu_ke         = $request->minggu_ke;
            $add->penyakit_menular  = $request->penyakit_menular;
            $add->mata_minus        = $request->mata_minus;
            $add->asma              = $request->asma;
            $add->jantung           = $request->jantung;
            $add->hipertensi        = $request->hipertensi;
            $add->diabetes          = $request->diabetes;
            $add->sesar             = $request->sesar;
            $add->p_previa          = $request->p_previa;
            $add->b_sungsang        = $request->b_sungsang;
            $add->b_kembar          = $request->b_kembar;
            $add->b_jantung_lemah   = $request->b_jantung_lemah;
            $add->fetal_distress    = $request->fetal_distress;
            $add->b_giant           = $request->b_giant;
            $add->save();

         return redirect('/redis')->with('status', 'Berhasil Menambah Data Rekam Medis Pasien');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $redis = Redis::find($id);
        return view ('redis.show', compact('redis'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $redis = Redis::find($id);
        return view ('redis.update', compact('redis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $edit = Redis::find($request->id);
        $edit->sistol            = $request->sistol;
        $edit->diastol           = $request->diastol;
        $edit->minggu_ke         = $request->minggu_ke;
        $edit->penyakit_menular  = $request->penyakit_menular;
        $edit->mata_minus        = $request->mata_minus;
        $edit->asma              = $request->asma;
        $edit->jantung           = $request->jantung;
        $edit->hipertensi        = $request->hipertensi;
        $edit->diabetes          = $request->diabetes;
        $edit->sesar             = $request->sesar;
        $edit->p_previa          = $request->p_previa;
        $edit->b_sungsang        = $request->b_sungsang;
        $edit->b_kembar          = $request->b_kembar;
        $edit->b_jantung_lemah   = $request->b_jantung_lemah;
        $edit->fetal_distress    = $request->fetal_distress;
        $edit->b_giant           = $request->b_giant;
        $edit->update();

     return redirect('/redis')->with('status', 'Berhasil Menambah Data Rekam Medis Pasien');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Redis::where('id', $id);
        $delete->delete();  

        return redirect('/redis')->with('status', 'Data Rekam Medis Pasien Delete');
    }
}