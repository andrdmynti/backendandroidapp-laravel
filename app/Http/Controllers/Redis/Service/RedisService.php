<?php

namespace App\Http\Controllers\Redis\Service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Redis;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class RedisService extends Controller
{
    public function getRedis()
    {

    	$query = Redis::all();
    	if(count($query) > 0)
    	{
    		$result = array('result' => 1, 'data' => $query);
    	}
    	else
    	{
    		$result = array('result' => 0, 'data' => 'null');	
    	}

    	return $result;

    }


    public function postRedisAdd()
    {

    	$input = Input::all();

    	$Validator = Validator::make(Input::all(),[
            'id'                => 'required',
    		'pasien_id'         => 'required',
            'bidan_id'          => 'required',
            // 'tekanan_darah'     => 'required',
            'sistol' 			=> 'required',
            'diastol' 			=> 'required',
            'minggu_ke'         => 'required',
            'penyakit_menular'  => 'required',
            'mata_minus'        => 'required',
            'asma'    	        => 'required',
            'jantung'           => 'required',
            'hipertensi'        => 'required',
            'diabetes'          => 'required',
            'sesar'             => 'required',
            'pinggul'           => 'required',
            'p_previa'          => 'required',
            'b_sungsang'        => 'required',
            'b_kembar'          => 'required',
            'b_jantung_lemah'   => 'required',
            'fetal_distress'    => 'required',
            'b_giant'           => 'required'
    	]);

    	if($Validator->fails())
    	{
    		$result = array("result" => 0, "error" => 'Data Not Complete', "errorField" => $Validator->errors());
    	}
    	else
    	{
    		try {

    			  $data  = Redis::create($input);
    			  if (count($data) > 0)
    			  {
    			  	  $result = array("result" => 1, "message" => 'Data Succsessful Entry', "data" => $data);
    			  }
    			  else
    			  {
    			  	  $result = array("result" => 0, "message" => 'Data Failed Entry', "data" => 'null'); 
    			  }
    			
    		} catch (Exception $e) {

    			$result = array("result" => 0, "message" => 'Data Failed Entry', "data" => $e->getMessage()." ".$e->getFile()." ".$e->getLine()); 
    			
    		}
    	
    	}

      return $result;

    }

    public function postRedisEdit()
    {

    	$input = Input::all();

    	$Validator = Validator::make(Input::all(),[
    		'id'                => 'required',
            'pasien_id'         => 'required',
            'bidan_id'          => 'required',
            // 'tekanan_darah'     => 'required',
            'sistol'            => 'required',
            'diastol'           => 'required',
            'minggu_ke'         => 'required',
            'penyakit_menular'  => 'required',
            'mata_minus'        => 'required',
            'asma'              => 'required',
            'jantung'           => 'required',
            'hipertensi'        => 'required',
            'diabetes'          => 'required',
            'sesar'             => 'required',
            'pinggul'           => 'required',
            'p_previa'          => 'required',
            'b_sungsang'        => 'required',
            'b_kembar'          => 'required',
            'b_jantung_lemah'   => 'required',
            'fetal_distress'    => 'required',
            'b_giant'           => 'required'
    	]);

    	if($Validator->fails())
    	{
    		$result = array("result" => 0, "error" => 'Data Not Complete', "errorField" => $Validator->errors());
    	}
    	else
    	{
    		try {

    			  $update  = Redis::find($input['id']);
    			  $update->update($input);

    			  if (count($update) > 0)
    			  {
    			  	  $result = array("result" => 1, "message" => 'Data Succsessful Edited', "data" => $update); 
    			  }
    			  else
    			  {
    			  	  $result = array("result" => 1, "message" => 'Data Failed Entry', "data" => 'null'); 
    			  }
    			
    		} catch (Exception $e) {

    			$result = array("result" => 1, "message" => 'Data Failed Entry', "data" => $e->getMessage()." ".$e->getFile()." ".$e->getLine()); 
    			
    		}
    	
    	}

      return $result;

    }
}
